#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <ctype.h>
#include <progbase/net.h>
 #include <sys/stat.h>
 #include <sys/types.h>
 #include <dirent.h>

#define BUFFER_LEN 1024

/**
Сервер містить фіксований масив строк. При старті сервера строки у масиві порожні.
Клієнт може присилати запити серверу на зміну строки по певному індексу в масиві:
	* create <folder> - створити директорію
	* delete <folder> - видалити директорію
	* showall - показати всі директорії
У відповідь на правильні запити сервер відправляє клієнту строку або масив строк, 
кожна строка у відповіді записується у окремому рядку
*/

typedef struct {
	char command[100];
	char file[1000];
	//int index;
	//char character;
} Request;

Request parseRequest(const char * msgStr) {
	Request req = {
		.command = "",
		.file = ""
	};

	int n = 0;
	while(isalpha(msgStr[n])) n++;

	strncpy(req.command, msgStr, n);
	req.command[n] = '\0';

	int j = 0;
	int i = 0;
	char * buffer = (char*)malloc(sizeof(char));
	for(i = n+1, j = 0; i < strlen(msgStr); i++, j++){
		buffer[j] = msgStr[i];
	}
	buffer[j-1] = '\0';
	strcpy(req.file, buffer);

	free(buffer);
	return req;
}

void printRequest(Request * req) {
	printf("Request: `%s` `%s`\n",
		req->command,
		req->file);
}

#define ARRAY_LEN 4
#define STRING_LEN 10

int main(void) {
	//
    // create UDP server
    UdpClient * server = UdpClient_init(&(UdpClient){});
    IpAddress * address = IpAddress_initAny(&(IpAddress){}, 9939);
    if (!UdpClient_bind(server, address)) {
        perror("Can't start server");
        return 1;
    }
    printf("Udp server started on port %d\n", 
        IpAddress_port(UdpClient_address(server)));
    
    NetMessage * message = NetMessage_init(
        &(NetMessage){},  // value on stack
        (char[BUFFER_LEN]){},  // array on stack 
        BUFFER_LEN);

    IpAddress clientAddress;
    while (1) {
        puts("Waiting for data...");
        //
        // blocking call to receive data
        // if someone send it
        if(!UdpClient_receiveFrom(server, message, &clientAddress)) {
			perror("recv");
			return 1;
		}

        printf("Received message from %s:%d (%d bytes): `%s`\n",
            IpAddress_address(&clientAddress),  // client IP-address
            IpAddress_port(&clientAddress),  // client port
            NetMessage_dataLength(message),
            NetMessage_data(message));

		const char * msgStr = NetMessage_data(message);
		Request req = parseRequest(msgStr);
		printRequest(&req);

		if(0 == strcmp(req.command, "create")){
			FILE * buf = fopen(req.file, "w");
			NetMessage_setDataString(message, "File created");
		}else if(0 == strcmp(req.command, "delete")){
			if(remove(req.file) != 0){
				NetMessage_setDataString(message, "File not found");
			}else{
				NetMessage_setDataString(message, "File deleted");
			}
		}else if(0 == strcmp(req.command, "showall")){
			DIR * dir;
			struct dirent *pDirent;
			if((dir = opendir (".")) == NULL){
				NetMessage_setDataString(message, "Cannot open .");
			}
			char * buffer = (char*)malloc(sizeof(char));
			char cpy[1000];
			while ((pDirent = readdir(dir)) != NULL) {
				
            	//printf ("[%s]\n", pDirent->d_name);
				sprintf(buffer, "[%s]\n", pDirent->d_name);
				strcat(cpy, buffer);
        	}
			NetMessage_setDataString(message, cpy);
			cpy[0] = '\0';
		}else{
			NetMessage_setDataString(message, "Error: unrecognized request");
		}

        //
        // send echo response
        if (!UdpClient_sendTo(server, message, &clientAddress)) {
			perror("send");
			return 1;
		}
    }
    //
    // close server
    UdpClient_close(server);
	return 0;
}