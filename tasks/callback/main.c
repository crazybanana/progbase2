#include <stdlib.h>
#include <stdio.h>

typedef struct{
    char name[20];
    float lvl_passed;
}Player;

int asc(const void * a, const void *b);
void printArr(int * arr, int count);
void printStruct(Player * Player, int count);
int struct_asc(const void * a, const void * b);
void Array_foreach(int arr[], int len, void (*action)(int, int));
void Array_foreachReversed(int arr[], int len, void (*action)(int, int));
void show_el(int el, int pos);

int main(void){
    Player player []= {
        {"User1", 4},
        {"User2", 7},
        {"User3", 6},
        {"User4", 12},
        {"User5", 2}
    };
    size_t structs_len = sizeof(player) / sizeof(Player);

    int arr[] = {4, 55, 43982, -3, -3232, 3343, 65, 9, 228, -34, -4};
    int length = sizeof(arr) / sizeof (arr[0]);

    printf("\nArray: ");
    printArr(arr, length);

    printf("Ascending by digits in numbers: ");
    qsort(arr, length, sizeof(arr[0]), asc);
    printArr(arr, length);

    printf("\nStruct: \n");
    printStruct(player, 5);

    printf("Ascending: \n");
    qsort(player, structs_len, sizeof(Player), struct_asc);
    printStruct(player, 5);
    printf("\n");

    printf("Array_foreach:\n");
    Array_foreach(arr, length, show_el);
    printf("Array_foreachReversed:\n");
    Array_foreachReversed(arr, length, show_el);
    printf("\n");
    return 0;
}

void show_el(int el, int pos){
    printf("El: %i, Pos: %i\n", el, pos);
}

void Array_foreachReversed(int arr[], int len, void (*action)(int, int)){
    for(int i = len-1; i > 0; i--){
        if((arr[i] % 2 == 0) && arr[i] < 0){
            action(arr[i], i);
        }
    }
}

void Array_foreach(int arr[], int len, void (*action)(int, int)){
    for(int i = 0; i < len; i++){
        if((arr[i] % 2 == 0) && arr[i] < 0){
            action(arr[i], i);
        }
    }
}

int asc(const void * a, const void * b) {
    int ia = *(int *)a;
    int ib = *(int *)b;

    int count = 1;
    int count1 = 1;
    
    while (ia = ia / 10){
        count++;
    }

    while(ib = ib / 10){
        count1++;
    }
    if(count > count1){
        return 1;
    }else if(count1 > count){
        return -1;
    }else return 0;
}

int struct_asc(const void * a, const void * b){
    Player *ia = (Player *)a;
    Player *ib = (Player *)b;
    return (int)(100.f*ia->lvl_passed - 100.f*ib->lvl_passed);
}

void printStruct(Player * Player, int count){
    for(int i = 0; i < count; i++){
        printf("Name: %s | Lvls: %.0f\n", Player[i].name, Player[i].lvl_passed);
    }
}

void printArr(int * arr, int count) {
    for (int i = 0; i < count; i++) {
   	 printf("%i ", arr[i]);
    }
    puts("");
}
