#ifndef RECTANGLE_H
#define RECTANGLE_H

#include <iostream>
#include <string>
#include <vector>

using namespace std;

class rectangle{

string m_name;
int m_heigth, m_length;
public:
rectangle(string _name, int _heigth, int _length);
~rectangle();
string get_name();
int get_heigth();
int get_length();
int get_area();
void print();
};

#endif