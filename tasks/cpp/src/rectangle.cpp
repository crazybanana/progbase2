#include "rectangle.h"
#include <iostream>
#include <string>
#include <vector>

using namespace std;

rectangle::~rectangle(){
	std::cout << "Rectangle is destoyed!" << std::endl;
}

rectangle::rectangle(string _name, int _length, int _heigth){
	m_name = _name;
	m_length = _length;
	m_heigth = _heigth;
}

string rectangle::get_name(){return m_name;}
int rectangle::get_heigth(){return m_heigth;}
int rectangle::get_length(){return m_length;}
int rectangle::get_area(){return m_length * m_heigth;}
void rectangle::print() {
    std::cout << m_name << " | " << m_length << " | " << m_heigth << " | " << std::endl;
}
