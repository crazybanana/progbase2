#include "list.h"
#include <stdlib.h>
#include <string.h>
#include "nlp.h"

struct Word{
    char letter;
};

struct Text{
    List * sentences;
};

struct Sentence{
    List * words;
};

struct Word * Word_New(char * letter){
    struct Word * self = malloc(sizeof(struct Word));
    self->letter = * letter;
    return self;
}

void Word_free(struct Word ** selfPtr){
    free(*selfPtr);
    *selfPtr = NULL;
}

char * Word_getLetter(struct Word * self, char * buffer){
    
    return buffer;
}

struct Text * Text_new(void){
    struct Text * self = malloc(sizeof(struct Text));
    self->sentences = List_new();
    return self;
}

void Text_free(struct Text ** selfPtr){
    struct Text * self = *selfPtr;
	List_free(&self->sentences);
	free(self);
	*selfPtr = NULL;
}

void Text_addSentence(struct Text * self, struct Sentence * sentence){
    List_add(self->sentences, sentence);
}

int Text_countSentence(struct Text * self){
    return List_count(self->sentences);
}

struct Sentence * Text_getSentence(struct Text * sel, int SentenceIndex){
    return (Sentence *)List_get(self->sentences, index);
}

struct Sentence * Sentence_new(void){
    struct Sentence * self = malloc(sizeof(struct Sentence));
    self->words = List_new();
    return self;
}


void Sentence_free(struct Sentence ** selfPtr) {
	struct Sentence * self = *selfPtr;
	List_free(&self->words);
	free(self);
	*selfPtr = NULL;
}

void Sentence_addWord(struct Sentence * self, struct Word * word){
    List_add(self->words, word);
}

int Sentence_countWord(struct Sentence * self){
    return List_count(self->words);
}

struct Word * Sentence_getWord(struct Sentence * self, int WordIndex){
    return (Word *)List_get(self->words, index);
}
