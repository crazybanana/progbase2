#ifndef FILE_H
#define FILE_H

#include "nlp.h"

Text * FileReadText(const char * filePath);
void FileWriteText(const char * filePath, Word * result);

#endif