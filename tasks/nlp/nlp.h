#ifndef LIST_H
#define LIST_H

#include <stdlib.h>
#include <stdbool.h>

typedef struct Text Text;
typedef struct Sentence Sentence;
typedef struct Word Word;

Text * Text_new(void);
void Text_free(Text ** selfPtr);
void Text_addSentence(Text * self, Sentence * sentence);
int Text_countSentence(Text * self);
Sentence * Text_getSentence(Text * self, int SentenceIndex);

Sentence * Sentence_new(void);
void Sentence_free(Sentence ** selfPtr);
void Sentence_addWord(Sentence * self, Word * word);
int Sentence_countWord(Sentence * self);
Word * Sentence_getWord(Sentence * self, int WordIndex);

bool Word_isStopWord(Word * self);
const char * Word_getString(Word * self);

#endif