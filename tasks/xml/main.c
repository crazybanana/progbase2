#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <libxml/parser.h>
#include <libxml/tree.h>


typedef struct{
    char team[20];
    char role[20];
} scrumDev;

typedef struct{
    char name[20];
    char surname[20];
    int year;
    float experience;
    scrumDev scrumDev;
} Student;

void XmlLoader_saveToString(char * str, const Student * students); 
void XmlLoader_loadFromString(Student * students, const char * xmlStr); 

int main(void){
    Student st = {
        .name = "Dima",
        .surname = "Ponomarchuk",
        .year = 1998,
        .experience = 3.6,
        .scrumDev = {
            .team = "Indians",
            .role = "Scrum Master",
        }
    };
    Student st1 = {
        .name = "",
        .surname = "",
        .year = 0,
        .experience = 0,
        .scrumDev = {
            .team = "",
            .role = "",
        }
    };
    char str[1000];
    XmlLoader_saveToString(str, &st);
	printf("%s", str);
    puts(" ");
    XmlLoader_loadFromString(&st1, str);
    printf("%s %s %d %.2f %s %s", st1.name, st1.surname, st1.year, st1.experience, st1.scrumDev.team, st1.scrumDev.role);
	puts("");
    return 0;
}

void XmlLoader_saveToString(char * str, const Student * students){
	xmlDoc * doc = NULL;
	xmlNode * rootNode = NULL;
    xmlNode * StudentNode = NULL;
    xmlNode * scrumDevNode = NULL;

    doc = xmlNewDoc(BAD_CAST "1.0");

    rootNode = xmlNewNode(NULL, BAD_CAST "Student");
    xmlDocSetRootElement(doc, rootNode);

    StudentNode = xmlNewChild(rootNode, NULL, BAD_CAST "Student", NULL);
    xmlNewProp(StudentNode, BAD_CAST "name", BAD_CAST students->name);
    xmlNewProp(StudentNode, BAD_CAST "surname", BAD_CAST students->surname);
    sprintf(str, "%i", students->year);
    xmlNewChild(StudentNode, NULL, BAD_CAST "year", BAD_CAST str);
    sprintf(str, "%.2f", students->experience); 
    xmlNewChild(StudentNode, NULL, BAD_CAST "experience", BAD_CAST str);

    scrumDevNode = xmlNewChild(StudentNode, NULL, BAD_CAST "scrumDev", NULL);
    xmlNewProp(scrumDevNode, BAD_CAST "name", BAD_CAST students->scrumDev.team);
    xmlNewChild(scrumDevNode, NULL, BAD_CAST "role", BAD_CAST students->scrumDev.role);

    xmlBuffer * bufferPtr = xmlBufferCreate();
    xmlNodeDump(bufferPtr, NULL, (xmlNode *)doc, 0, 1);
    sprintf(str, "%s", (const char *)bufferPtr->content);
    puts(" ");
    xmlFreeDoc(doc);
}

void XmlLoader_loadFromString(Student * students, const char * xmlStr){
        xmlDoc * xDoc = xmlReadMemory(xmlStr, strlen(xmlStr), NULL, NULL, 0);
        xmlNode * xRootEl = xmlDocGetRootElement(xDoc);
        for(xmlNode * xCur = xRootEl->children; NULL != xCur ; xCur = xCur->next) {
            if (XML_ELEMENT_NODE == xCur->type) {
                char * name = (char *)xmlGetProp(xCur, BAD_CAST "name");
                strcpy(students->name, (char *)name);
                xmlFree(name);
                char * surname = (char *)xmlGetProp(xCur, BAD_CAST "surname");
                strcpy(students->surname, (char *)surname);
                xmlFree(surname);
                for (xmlNode * xJ = xCur->children; NULL != xJ ; xJ = xJ->next) {
                    if (XML_ELEMENT_NODE == xJ->type) {
                        if(strcmp((char *)xJ->name, "year") == 0){
                            char * content = (char *)xmlNodeGetContent(xJ);
                            students->year = atoi((char *)content);
                            xmlFree(content);
                        } else if(strcmp((char *)xJ->name, "experience") == 0){
                            char * content = (char *)xmlNodeGetContent(xJ);
                            students->experience = atof((char *)content);
                            xmlFree(content);
                        } else if(strcmp((char *)xJ->name, "scrumDev") == 0){
                            char * name = (char *)xmlGetProp(xJ, BAD_CAST "name");
                            strcpy(students->scrumDev.team, (char *)name);
                            xmlFree(name);
                            char * role = (char *)xmlNodeGetContent(xJ);
                            strcpy(students->scrumDev.role, (char *)role);
                            xmlFree(role);
                        }
                    }
                }
            }
        }
        xmlFreeDoc(xDoc);
}