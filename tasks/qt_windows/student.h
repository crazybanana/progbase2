#ifndef STUDENT_H
#define STUDENT_H

#include <QMetaType>
#include <QString>

class Student
{
private:
    QString _name = "";
    QString _surname = "";
    int _age = 0;
public:
    Student();
    Student(QString, QString, int);
    ~Student();

    QString getSurname();
    QString getName();
    int getAge();
    void print();
};

Q_DECLARE_METATYPE(Student)

#endif // STUDENT_H
