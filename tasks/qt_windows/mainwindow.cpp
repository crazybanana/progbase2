#include <QDebug>
#include <QCloseEvent>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "student.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked(){
    Student student[3];
    int index;
    if(ui->label_3->text() == "NULL" || ui->label_3->text() == "1"){
        index = 2;
    }else{
        index = ui->label_3->text().toInt() - 2;
    }

    student[0]._name = (QString)"asdsada";
    student[0]._surname = (QString)"asdsa121";
    student[0]._age = 12;
    student[1]._name = (QString)"asdsada";
    student[1]._surname = (QString)"asdsa121";
    student[1]._age = 16;
    student[2]._name = (QString)"asdsada";
    student[2]._surname = (QString)"asdsa121";
    student[2]._age = 19;

    ui->nameLineEdit->setText(student[index].getName());
    ui->surNameLineEdit->setText(student[index].getSurname());
    ui->spinBox->setValue(student[index].getAge());
    index--;
    ui->label_3->setText(QString::number(index + 2));
}

void MainWindow::on_pushButton_2_clicked(){
    Student student[3];
    int index;
    if(ui->label_3->text() == "NULL" || ui->label_3->text() == "3"){
        index = 0;
    }else{
        index = ui->label_3->text().toInt();
    }

    student[0]._name = (QString)"asdsada";
    student[0]._surname = (QString)"asdsa121";
    student[0]._age = 12;
    student[1]._name = (QString)"asdsada";
    student[1]._surname = (QString)"asdsa121";
    student[1]._age = 16;
    student[2]._name = (QString)"asdsada";
    student[2]._surname = (QString)"asdsa121";
    student[2]._age = 19;


    ui->nameLineEdit->setText(student[index].getName());
    ui->surNameLineEdit->setText(student[index].getSurname());
    ui->spinBox->setValue(student[index].getAge());
    index++;
    QString temp = QString::number(index);
    ui->label_3->setText(temp);
}

