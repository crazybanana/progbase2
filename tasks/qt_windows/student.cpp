#include "student.h"
#include <iostream>

Student::Student() { }

Student::Student(QString name, QString surname, int age)
{
    this->_name = name;
    this->_surname = surname;
    this->_age = age;
}

Student::~Student() { }

QString Student::getSurname(){return this->_surname;}
QString Student::getName(){return this->_name;}
int Student::getAge(){return this->_age;}


void Student::print()
{
    std::cout << _name.toStdString() << " | " << _surname.toStdString() << " | " << _age << std::endl;
}
