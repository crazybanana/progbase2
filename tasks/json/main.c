#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <jansson.h>

typedef struct {
	char * team;
	char * role;
} scrumDev;

typedef struct {
	char * name;
	char * surname;
	int  year;
	float score; 
	scrumDev scrumDev;
} Student;

void readAllText(const char * filename, char * text) {
	char line[100];
    
    FILE * fr = fopen(filename, "r");
    while(fgets(line, 100, fr)) {
        strcat(text, line);
    }
	fclose(fr);
}

void JsonLoader_saveToString(char * str, const Student * students){
	json_t * json = json_object();
	json_object_set_new(json, "name", json_string(students->name));
	json_object_set_new(json, "surname", json_string(students->surname));
	json_object_set_new(json, "year", json_integer(students->year));
	json_object_set_new(json, "score", json_real(students->score));
	json_t * scrumDevObj = json_object();
	json_object_set_new(scrumDevObj, "team", json_string(students->scrumDev.team));
	json_object_set_new(scrumDevObj, "role", json_string(students->scrumDev.role));
	json_object_set_new(json, "scrumDev", scrumDevObj);

	char * jsonString = json_dumps(json, JSON_INDENT(2));
	puts(jsonString);
	free(jsonString);

	json_decref(json);
}

void JsonLoader_loadFromString(Student * students, const char * jsonString){
	int studentsCount = 0;

	json_error_t err;
	json_t * jsonArr = json_loads(jsonString, 0, &err);
	int index = 0;
	json_t * value = NULL;
	json_array_foreach(jsonArr, index, value) {
		json_t * scrumDevObj = json_object_get(value, "scrumDev");
		students[index] = (Student) {
			.name = (char *)json_string_value(json_object_get(value, "name")),
			.surname = (char *)json_string_value(json_object_get(value, "surname")),
			.year = json_integer_value(json_object_get(value, "year")),
			.score = json_real_value(json_object_get(value, "score")),
			.scrumDev = (scrumDev) {
				.team = (char *)json_string_value(json_object_get(scrumDevObj, "team")),
				.role = (char *)json_string_value(json_object_get(scrumDevObj, "role"))
			}
		};
		studentsCount++;
	}

	for (int i = 0; i < studentsCount; i++) {
		Student * st = &students[i];
	    printf("\n\nStudent %i:\n", i);
		printf("\t%10s: %s\n", "name", st->name);
		printf("\t%10s: %s\n", "surname", st->surname);
		printf("\t%10s: %i\n", "year", st->year);
		printf("\t%10s: %f\n", "score", st->score);
		printf("\t%10s: %s (%s)\n", "scrumDev", 
			st->scrumDev.team, 
			st->scrumDev.role);
	}

	json_decref(jsonArr);
}

int main(void) {
	Student st = {
		.name = "Dima",
		.surname = "Ponomarchuk",
		.year = 1998,
		.score = 3.7,
		.scrumDev = {
			.team = "Indians",
			.role = "Scrum Master",
		}
	};

	Student st1 = {
		.name = "",
		.surname = "",
		.year = 0,
		.score = 0,
		.scrumDev = {
			.team = "",
			.role = "",
		}
	};
	char jsonStr[1000] = "";
	readAllText("students.json", jsonStr);

	char buffer[1000];
	puts("");
	JsonLoader_saveToString(buffer, &st);
	puts("");
	JsonLoader_loadFromString(&st1, jsonStr);
	puts("");
	return 0;
}