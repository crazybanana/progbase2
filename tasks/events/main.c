#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <list.h>
#include <events.h>
#include <pbconsole.h>
#include <progbase.h>
#include <string.h>
#include <ctype.h>


enum {
	DigitFromTextEventTypeId,
	TimerElapsedEventTypeId
};

void ReadTextHandler_update(EventHandler * self, Event * event);
bool readAllText(const char * fileName, char * text);

typedef struct Task {
	//double filter;
	double sum;
} Task;

Task * Timer_new() {
	Task * self = malloc(sizeof(Task));
	self->sum = 0;
	return self;
}

void Timer_free(Task * self) {
	free(self);
}

void Timer_secondHandleEvent(EventHandler * self, Event * event) {
	Task * task = (Task *)self->data;
	if (event->type == DigitFromTextEventTypeId) {
			int * num = (int *)event->data;
			task->sum += *num;
			printf("New Number >> %d\n", *num);
			if (task->sum > 50) {
				printf("SUM is 50 or more>> %f\n", task->sum);
				task->sum = 0;
			}
			sleepMillis(100);
	}
}

void Timer_firstHandleEvent(EventHandler * self, Event * event) {
	Task * task = (Task *)self->data;
	if (event->type == DigitFromTextEventTypeId) {
			int * num = (int *)event->data;
			task->sum += *num;
			printf("New Number >> %d\n", *num);
			if (task->sum > 30) {
				printf("SUM is 30 or more>> %f\n", task->sum);
				task->sum = 0;
			}
			sleepMillis(100);
	}
}

int main(void) {
	EventSystem_init();

	char * fileName = malloc(sizeof(char) * 100);
	strcpy(fileName, "text.txt");
	EventSystem_addHandler(EventHandler_new(fileName, free, ReadTextHandler_update));
	EventSystem_addHandler(EventHandler_new(Timer_new(), (DestructorFunction)Timer_free, Timer_firstHandleEvent));
	EventSystem_addHandler(EventHandler_new(Timer_new(), (DestructorFunction)Timer_free, Timer_secondHandleEvent));


	// start infinite event loop
	EventSystem_loop();
	// cleanup event system
	EventSystem_cleanup();
	return 0;
}


bool readAllText(const char * fileName, char * text){
	char line[1000];
	FILE * fr = fopen(fileName, "r");
	if(fr == NULL) return false;
	while(fgets(line,100,fr)){
		strcat(text, line);
	}
	fclose(fr);
	return true;
}

void ReadTextHandler_update(EventHandler *self, Event *event) {
	if (event->type == StartEventTypeId) {
		char buffer[10000] = "";
		char * filename = (char *)self->data;
		readAllText(filename, buffer);
		if (!strcmp(buffer, "")) {
			puts("File does not exist or empty");
			EventSystem_exit();
		}
		for(int i = 0; i < strlen(buffer); i++){
			char ch = buffer[i];
			if(isdigit(ch)){
				int * number = malloc(sizeof(int));
				*number = atoi(&ch);
				EventSystem_raiseEvent(Event_new(self,DigitFromTextEventTypeId, number, free));
			}
		}
	}
}