#include <stdlib.h>
#include <string.h>
#include <array.h>

void Array_copy(
    Array sourceArray,
    int sourceIndex,
    Array destinationArray,
    int destinationIndex,
    int length
) {
    // @todo add checks
    size_t itemSize = sourceArray.itemSize;
    size_t copySize = itemSize * length;
    char buffer[copySize];
    memcpy(buffer, sourceArray.items + (sourceIndex) * itemSize, copySize);
    memcpy(destinationArray.items + (destinationIndex) * itemSize, buffer, copySize);
}