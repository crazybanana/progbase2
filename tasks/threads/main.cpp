#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <thread>
#include <mutex>

#include <progbase.h>
#include <progbase-cpp/console.h>

#include <graphics.h>

using namespace std;
using namespace progbase::console;

void print(char & symb, CursorAttributes color,  mutex & g_mutex) {
	while (1) {
		g_mutex.lock();
		Console::setCursorAttribute(color);
		cout << symb << flush;
		g_mutex.unlock();
		this_thread::sleep_for(chrono::milliseconds(100));
	}
}

void drawCircle() {

	// g_m.lock();
	// double rad = 5 * time * M_PI / 180.0;
	// time++;
	// clearRect((Vec2D){0, 0}, (Vec2D){40, 24});
	// Graphics_drawCircle(NULL, (Vec2D){20, 12}, 5 + 3 * cosf(rad), CursorAttributes::BG_YELLOW);
	// g_m.unlock();
}

mutex m;

void printRotatedCircle(
	Vec2D consoleCenter, 
	double rotateRadius,
	double radius,
	double amp,
	double radians,
	double dir
	) 
{
	double r = amp * sinf(radians);
	Vec2D rotateVector = (Vec2D){1, 1};
	rotateVector = Vec2D_normalize(rotateVector);
	rotateVector = Vec2D_multByNumber(rotateVector, rotateRadius);
	rotateVector = Vec2D_rotate(rotateVector, dir * radians);
	Vec2D circleCenter = Vec2D_add(consoleCenter, rotateVector);
	m.lock();
	Graphics_drawCircle(
		NULL, 
		circleCenter, 
		radius + r, 
		CursorAttributes::BG_YELLOW);
	m.unlock();
}

void threadf(double & dir) {
	Vec2D consoleCenter = (Vec2D){40, 12};
	double rotateRadius = 5;
	const int amp = 2;
	double radius = 5;
	int degrees = 0;
	while (1) {
		m.lock();
		Console::setCursorAttribute(CursorAttributes::BG_DEFAULT);
		Console::clear();
		m.unlock();
		double radians = degrees * M_PI / 180.0;
		printRotatedCircle(
			consoleCenter,
			rotateRadius,
			radius,
			amp,
			radians,
			dir
		);
		degrees++;
	}
}

int main(void) {

	double d1 = -2;
	double d2 = 3;
	thread t1(threadf, ref(d1));
	thread t2(threadf, ref(d2));

	int x;
	cin >> x;

	return 0;
}

void printRotatedSQ(
Vec2D consoleCenter, 
double rotateRadius,
double radius,
double amp,
double radians,
double dir
) 
{
	double r = amp * sinf(radians);
	Vec2D rotateVector = (Vec2D){1, 1};
	rotateVector = Vec2D_normalize(rotateVector);
	rotateVector = Vec2D_multByNumber(rotateVector, rotateRadius);
	Vec2D rotateVector1 = Vec2D_rotate(rotateVector, dir * radians);
	Vec2D rotateVector2 = Vec2D_rotate(rotateVector, dir * radians - (M_PI / 2.0));
	Vec2D rotateVector3 = Vec2D_rotate(rotateVector, dir * radians + M_PI);
	Vec2D rotateVector4 = Vec2D_rotate(rotateVector, dir * radians - (270.0 * M_PI / 180.0));
	Vec2D v1 = Vec2D_add(consoleCenter, rotateVector1);
	Vec2D v2 = Vec2D_add(consoleCenter, rotateVector2);
	Vec2D v3 = Vec2D_add(consoleCenter, rotateVector3);
	Vec2D v4 = Vec2D_add(consoleCenter, rotateVector4);
	m.lock();
	Graphics_drawLine(
		NULL,
		v1,
		v2,
		CursorAttributes::BG_BLUE
	);
	Graphics_drawLine(
		NULL,
		v2,
		v3,
		CursorAttributes::BG_BLUE
	);
	Graphics_drawLine(
		NULL,
		v3,
		v4,
		CursorAttributes::BG_BLUE
	);
	Graphics_drawLine(
		NULL,
		v4,
		v1,
		CursorAttributes::BG_BLUE
	);
	m.unlock();
}