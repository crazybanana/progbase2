/** @file
@brief - defines Server
*/
#ifndef HTTP_SERVER_H
#define HTTP_SERVER_H

#include <iostream>
#include <vector>
#include <actor.h>
using namespace std;
/**
@brief - HTTP server
*/
void http_server(void);
/**
@brief - HTTP server
@param message data from client
@param actors
@return string response from server
*/
string createResponse(string message, vector<Actor *> actors);
/**
@brief creates information about server
@return response to "/"
*/
string serverInformation(void);
/**
@brief creates information about favorite actors
@param actors my favorite actors
@return response on "/favorites"
*/
string myFavouriteActors(vector<Actor *> actors);
/**
@brief creates information about some actors, by field key = value
@param actors my favorite actors
@param key name of field
@param value value of field
@return response on /favorites?somefield=value and /favorites/id
*/
string keyActors(vector<Actor *> actors, string key, string value);
/**
@brief creates information about file "data.txt"
@return response to /file
*/
string fileInformation(void);
/**
@brief creates information about numbers in file "data.txt"
@return response to /file/data
*/
string fileNumber(void);
/**
@brief reads file
@param filePath path to the file and its name
@return information from file
*/
string readFile(const char *filePath);

#endif // HTTP_SERVER_H
