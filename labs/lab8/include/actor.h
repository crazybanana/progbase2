/**
    @file
    @brief Data about favorite actors
*/
#ifndef SINGER_H
#define SINGER_H

#include <iostream>
#include <vector>
using namespace std;
/**
    @class Actor
    @brief defines a favorite actor
*/
class Actor
{
    string _name;
    string _surname;
    string _hometown;
    int _awards;
    int _id;
public:
 /**
    @brief default public constructor for Actor
 */
    Actor();
/**
    @brief public constructor for Actor that fill all fields in Actor
    @param name - unique name of actor
    @param surname - actor`s surname
    @param hometown - actor`s hometown
    @param awards - awards of actor`s hometown
    @param awards - unique actor`s id
*/
    Actor(
        string name,
        string surname,
        string hometown,
        int awards,
        int id
        );
 /**
    @brief default public destructor for Actor
*/
    ~Actor();
 /** 
    @brief sets name of actor 
    @param name name of actor 
*/
    void setName(string name);
/** 
    @brief sets surname of actor`s hometown 
    @param surname surname of actor`s hometown 
*/
    void setGenre(string surname);
/** 
    @brief sets name of actor`s hometown 
    @param hometown hometown of actor `s hometown
*/
    void setSong(string hometown);
 /** 
    @brief sets awards of actor`s hometown  
    @param awards awards of actor`s hometown 
*/    
    void setDuration(int awards);
/** 
    @brief sets id of actor`s hometown 
    @param id id of actor`s hometown  
*/
    void setId(int id);
/**
    @brief get name of actor
    @return string that represents name of actor
*/
    string getName(void);
/**
    @brief get surname of actor`s hometown
    @return string that represents surname of actor`s hometown
*/
    string getGenre(void);
 /**
    @brief get hometown of actor
    @return string that represents actor`s hometown
*/    
    string getSong(void);
/**
    @brief get awards of actor`s hometown
    @return string that represents awards of actor`s hometown
*/
    int getDuration(void);
/**
    @brief get actor`s id
    @return string that represents unique actor`s id
*/
    int getId(void);
/**
    @brief get value that contain specified filed of Actor
    @param field - string that defines field which value returns
    @return string that represents value at specified field
*/
    string getField(string field);
};
/**
    @brief sets some default Actor objects 
*/
void actorsInit(vector<Actor*> &actors);

#endif // SINGER_H
