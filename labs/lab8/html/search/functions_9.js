var searchData=
[
  ['serverinformation',['serverInformation',['../http__server_8h.html#a7a25181b5addc5b7c896755fdca21659',1,'http_server.cpp']]],
  ['setcommand',['setCommand',['../classRequest.html#a4fe67f0c9cccfd530174eb3d8fb54013',1,'Request']]],
  ['setduration',['setDuration',['../classActor.html#a9464945bfd339e534f09969cf9f136e9',1,'Actor']]],
  ['setgenre',['setGenre',['../classActor.html#a3ee8568dfed8ac9824bb2c08778712f3',1,'Actor']]],
  ['setid',['setId',['../classActor.html#ae34c52525dd4f4cb10628dc6931da308',1,'Actor']]],
  ['setkey',['setKey',['../classRequest.html#a56210902bb0d240b4615d6fde144ea42',1,'Request']]],
  ['setname',['setName',['../classActor.html#a985cdf56b255f72d05a238f59b324d0b',1,'Actor']]],
  ['setsong',['setSong',['../classActor.html#ad0eaf1857407108ba9c6b48e8d107d3c',1,'Actor']]],
  ['seturl',['setURL',['../classRequest.html#a1a46a4b8f73d3eb03c39ada91b26eeed',1,'Request']]],
  ['setvalue',['setValue',['../classRequest.html#a2f8dbfdcbf1d9f07e8d1aa7216b044e3',1,'Request']]]
];
