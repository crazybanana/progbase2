var searchData=
[
  ['getcommand',['getCommand',['../classRequest.html#a95ef26627def0c193c332b9bfd4ebf8e',1,'Request']]],
  ['getduration',['getDuration',['../classActor.html#a13ec75cb3e8518a5b169565644a04995',1,'Actor']]],
  ['getfield',['getField',['../classActor.html#accda715da1a9b04bc7676a0b409f76b8',1,'Actor']]],
  ['getgenre',['getGenre',['../classActor.html#a6472fd71302b068e571f6d46da718c7a',1,'Actor']]],
  ['getid',['getId',['../classActor.html#abeca6fc11a0caff763192d5fdc1d14fa',1,'Actor']]],
  ['getkey',['getKey',['../classRequest.html#aef5c14196e6a7c021f3797fc2a43473c',1,'Request']]],
  ['getname',['getName',['../classActor.html#a765c9662fa1c4c7d6f6528e438e676ed',1,'Actor']]],
  ['getsong',['getSong',['../classActor.html#a1ce19e43fb3197d272419403407cd57e',1,'Actor']]],
  ['geturl',['getURL',['../classRequest.html#a49d0bafc034c350c8af8c3bb666e0099',1,'Request']]],
  ['getvalue',['getValue',['../classRequest.html#adf447291d428212c0c6d93e4c72fb127',1,'Request']]]
];
