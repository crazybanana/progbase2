#include "actor.h"
using namespace std;
Actor::Actor() {}

Actor::Actor(string name, string surname, string hometown, int awards, int id)
{
    _name = name;
    _surname = surname;
    _hometown = hometown;
    _awards = awards;
    _id = id;
}

Actor::~Actor() {}

void Actor::setName(string name)
{
    _name = name;
}

void Actor::setGenre(string surname)
{
    _surname = surname;
}

void Actor::setSong(string hometown)
{
    _hometown = hometown;
}

void Actor::setDuration(int awards)
{
    _awards = awards;
}

void Actor::setId(int id)
{
    _id = id;
}

string Actor::getName()
{
    return _name;
}

string Actor::getGenre()
{
    return _surname;
}

string Actor::getSong()
{
    return _hometown;
}

int Actor::getDuration()
{
    return _awards;
}

int Actor::getId()
{
    return _id;
}

string Actor::getField(string field)
{
    if (field == "name") return _name;
    if (field == "surname") return _surname;
    if (field == "hometown") return _hometown;
    if (field == "awards") return to_string(_awards);
    if (field == "id") return to_string(_id);
    return NULL;
}

void actorsInit(vector<Actor*> &actors)
{
    actors.push_back(new Actor("Мэттью", "Макконехи","Ювалде", 74, 1));
    actors.push_back(new Actor("Энн", "Хэтэуэй","Бруклин", 4, 2));
    actors.push_back(new Actor("Джесика", "Честейн","Сонома", 2, 3));
    actors.push_back(new Actor("Мэтт", "Деймон","Кэмбридж", 1., 4));
    actors.push_back(new Actor("Райан", "Рейнольдс","Ванкувер", 3, 5));
    actors.push_back(new Actor("Джеймс", "Макэвой","Глазго", 3, 6));
}
