#ifndef COMPOSITION_H
#define COMPOSITION_H

#include <string>
#include <sstream>
#include <iostream>
#include <cstring>
#include <curlpp/cURLpp.hpp>
#include <curlpp/Easy.hpp>
#include <curlpp/Options.hpp>
#include <QByteArray>
#include <QFile>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>
#include <QVariant>
#include <QDebug>


class composition
{
    QString name;
    QString born;
    QString died;
    QString gender;
public:
    composition();
    composition(QString name, QString born, QString died, QString gender);
    ~composition();

    QString getName();
    QString getBorn();
    QString getDied();
    QString getGender();

    void setFields(QString name, QString born, QString died, QString gender);
};

#endif // COMPOSITION_H
