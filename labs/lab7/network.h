#ifndef NETWORK_H
#define NETWORK_H

#include <string>
#include <sstream>
#include <iostream>
#include <cstring>
#include <curlpp/cURLpp.hpp>
#include <curlpp/Easy.hpp>
#include <curlpp/Options.hpp>
#include <QByteArray>
#include <QFile>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>
#include <QVariant>
#include <QDebug>

class network
{
public:
    network();
    std::string getJson(std::string str);
};

#endif // NETWORK_H
