#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "network.h"

using namespace std;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushButton_clicked()
{
    try {
        network request;
        for(int i = 500; i < 600; i++){
            string buff = "https://anapioficeandfire.com/api/characters/";
            string msg = request.getJson(buff.append(to_string(i)));
            parseJsonToStdVector(msg);
        }
    } catch( curlpp::RuntimeError &e ) {
        std::cout << e.what()<< std::endl;
    } catch( curlpp::LogicError &e ) {
        std::cout << e.what() << std::endl;
    }

    ui->pushButton->setEnabled(false);
}


void MainWindow::on_listWidget_itemSelectionChanged()
{
    QList<QListWidgetItem *> items = ui->listWidget->selectedItems();
    bool hasSelected = items.size() > 0;
    if (hasSelected) {
        int index = ui->listWidget->currentRow();
        composition * obj = parray.at(index);

        ui->label_3->setText(obj->getName());
        ui->label_4->setText(obj->getBorn());
        ui->label_5->setText(obj->getDied());
        ui->label_7->setText(obj->getGender());
    }
}

void MainWindow::on_pushButton_2_clicked()
{
    ui->listWidget_2->clear();
    QString text = ui->lineEdit->text();
    QString status = ui->comboBox->currentText();
    if(status == "Gender"){
        for(composition * ob : parray){
            if(text == ob->getGender()){
                QListWidgetItem * qCharacterListItem = new QListWidgetItem();
                qCharacterListItem->setText(ob->getName());
                ui->listWidget_2->addItem(qCharacterListItem);
            }
        }
    }
    if(status == "Dead/Alive"){
        for(composition * ob : parray){
            if(text == "Alive"){
                if(ob->getDied() == ""){
                    QListWidgetItem * qCharacterListItem = new QListWidgetItem();
                    qCharacterListItem->setText(ob->getName());
                    ui->listWidget_2->addItem(qCharacterListItem);
                }
            }else if("Dead"){
                if(ob->getDied() != ""){
                    QListWidgetItem * qCharacterListItem = new QListWidgetItem();
                    qCharacterListItem->setText(ob->getName());
                    ui->listWidget_2->addItem(qCharacterListItem);
                }
            }
        }
    }
}

void MainWindow::parseJsonToStdVector(std::string str){
    QJsonDocument itemDoc = QJsonDocument::fromJson(str.c_str());
    QJsonObject itemObject = itemDoc.object();

    composition * item = new composition();
    item->setFields(itemObject["name"].toString(), itemObject["born"].toString(), itemObject["died"].toString(), itemObject["gender"].toString());
    QListWidgetItem * qCompositionListItem = new QListWidgetItem();
    qCompositionListItem->setText(item->getName());
    ui->listWidget->addItem(qCompositionListItem);
    parray.push_back(item);
}

