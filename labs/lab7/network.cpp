#include "network.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"

using namespace std;

network::network() {}

string network::getJson(std::string str){
    curlpp::Cleanup myCleanup;

    curlpp::options::Url myUrl(str);
    curlpp::Easy myRequest;
    myRequest.setOpt(myUrl);

    std::ostringstream os;
    os << myRequest;

    return os.str();
}
