#-------------------------------------------------
#
# Project created by QtCreator 2017-05-15T10:40:38
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = lab7
TEMPLATE = app
CONFIG += c++11


SOURCES += main.cpp\
        mainwindow.cpp \
    composition.cpp \
    network.cpp

HEADERS  += mainwindow.h \
    composition.h \
    network.h

FORMS    += mainwindow.ui

LIBS += -lcurlpp -lcurl -ljson

