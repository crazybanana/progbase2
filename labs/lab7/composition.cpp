#include "composition.h"
#include "QString"

composition::composition() {}

composition::composition(QString name, QString born, QString died, QString gender)
{
    this->name = name;
    this->born = born;
    this->died = died;
    this->gender = gender;
}

composition::~composition() { }

QString composition::getName() {return this->name;}

QString composition::getBorn() {return this->born;}

QString composition::getDied(){return this->died;}

QString composition::getGender(){return this->gender;}

void composition::setFields(QString name, QString born, QString died, QString gender) {
    this->name = name;
    this->born = born;
    this->died = died;
    this->gender = gender;
}

