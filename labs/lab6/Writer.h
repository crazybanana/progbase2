#ifndef WRITER_H
#define WRITER_H

#include <QString>

class Writer
{
    QString name;
    QString surname;
    int age;
public:
    Writer();
    Writer(QString name, QString surname, int age);
    ~Writer();

    QString getName();
    QString getSurname();
    int getAge();

    void setFields(QString name, QString surname, int age);
};

#endif // WRITER_H
