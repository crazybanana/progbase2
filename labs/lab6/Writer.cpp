#include "Writer.h"
#include <iostream>

using namespace std;

Writer::Writer(){}

Writer::~Writer(){}

Writer::Writer(QString name, QString surname, int age){
    this->name = name;
    this->surname = surname;
    this->age = age;
}

QString Writer::getName(){return this->name;}
QString Writer::getSurname(){return this->surname;}
int Writer::getAge(){return this->age;}

void Writer::setFields(QString name, QString surname, int age){
    this->name = name;
    this->surname = surname;
    this->age = age;
}
