#include "dialog.h"
#include "ui_dialog.h"
#include "Writer.h"
#include <QDialog>
#include <QErrorMessage>

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
}

Dialog::~Dialog()
{
    delete ui;
}

Writer * Dialog::on_pushButton_clicked(){
    Writer * newWriter = new Writer;
    newWriter->setFields(ui->lineEdit->text(), ui->lineEdit_2->text(), (ui->spinBox->value()));

    if(ui->lineEdit->text().isEmpty() || ui->lineEdit_2->text().isEmpty()){
        (new QErrorMessage(this))->showMessage("One of important fields is empty!");
    }else{
        this->accept();
    }
    return newWriter;
}

void Dialog::on_pushButton_2_clicked(){
    this->reject();
}

