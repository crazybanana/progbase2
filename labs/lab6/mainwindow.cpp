#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include "Writer.h"
#include "dialog.h"
#include <QVariant>
#include <iostream>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_2_clicked()
{
    Dialog dialog;
    dialog.setWindowTitle("New Writer");

    if(dialog.exec()){
        Writer * writer = dialog.on_pushButton_clicked();
        QListWidgetItem * qWriterListItem = new QListWidgetItem();
        qWriterListItem->setText(writer->getSurname());
        ui->listWidget->addItem(qWriterListItem);
        parray.push_back(writer);
    }else{
        qDebug() << "Rejected!";
    }
}

void MainWindow::on_pushButton_clicked()
{
    QList<QListWidgetItem *> items = ui->listWidget->selectedItems();
    if(items.size() > 0){
        int index = ui->listWidget->currentRow();
        ui->label_7->clear();
        ui->label_8->clear();
        ui->label_9->clear();
        delete ui->listWidget->takeItem(ui->listWidget->row(items[0]));
        parray.erase(parray.begin() + index);
    }
}

void MainWindow::on_listWidget_itemSelectionChanged()
{
    QList<QListWidgetItem *> items = ui->listWidget->selectedItems();
    if(items.size() > 0){
        int index = ui->listWidget->currentRow();
        Writer * writer = parray.at(index);
        ui->label_7->setText(writer->getName());
        ui->label_8->setText(writer->getSurname());
        ui->label_9->setText(QVariant(writer->getAge()).toString());
        ui->pushButton->setEnabled(true);
    }else{
        ui->pushButton->setEnabled(false);
    }
}

void MainWindow::on_pushButton_3_clicked()
{
   ui->listWidget_2->clear();
   ui->label_10->clear();
   int buff = ui->spinBox->value();
   int i = 0;
   for(Writer * writer : parray){
       if(buff > writer->getAge()){
           QListWidgetItem *qWriterListItem = new QListWidgetItem();
           qWriterListItem->setText(writer->getSurname());
           ui->listWidget_2->addItem(qWriterListItem);
           i++;
       }
   }
   QString s = QString::number(i);
   if(i != 0)
   ui->label_10->setText(s);
}
