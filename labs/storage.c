#include <storage.h>
#include <list.h>
#include <convert.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include <jansson.h>
#include <libxml/parser.h>
#include <libxml/tree.h>

#define throw(MSG) assert(0 && MSG);

struct Course{
    char courseName[50];
    int numberSubjects;
};

struct Lector{
    char name[100];
    char surname[100];
    int bornYear;
    float salary;
    Course course;
};

List * Storage_load(const char * fileName){
	List * list = List_new();

	char buffer[1000] = "";
	readAllText(fileName, buffer);

	char * str = strtok(buffer, "\n\0");

	while(str != NULL){
		List_add(list, Lector_newFromString(str));
		str = strtok (NULL, "\n\0");
	}
	return list;
}

bool readAllText(const char * fileName, char * text){
	char line[1000];
	FILE * fr = fopen(fileName, "r");
	if(fr == NULL) return false;
	while(fgets(line,100,fr)){
		strcat(text, line);
	}
	fclose(fr);
	return true;
}

bool Storage_save(const char * fileName, List * lector){
	FILE * fp = fopen(fileName, "w");
	if(fp == NULL){
		return false;
	}

	printf("List saved to %s\n", fileName);
	char buffer[100];
	for (int i = 0; i < List_count(lector); i++) {
		Lector * c = (Lector *)List_get(lector, i);
		char * strPtr = Lector_toString(c, buffer);
		printf("\t%i) %s\n", i+1, strPtr);
		fprintf(fp, "%s\n", strPtr);
	}
	fclose(fp);
	return true;
}

bool JsonLoader_saveToString(const char * fileName, List * Lector){
    FILE * fp = fopen(fileName, "w");
    if(fp == NULL){
        return false;
    }
    json_t * arr = json_array();

    for(int i = 0; i < List_count(Lector); i++){
        json_t * json = json_object();

        json_object_set_new(json, "Name", json_string(Lector_getName(List_get(Lector, i))));
        json_object_set_new(json, "Surname", json_string(Lector_getSurname(List_get(Lector, i))));
        json_object_set_new(json, "bornYear", json_integer(Lector_getYear(List_get(Lector, i))));
        json_object_set_new(json, "Salary", json_real(Lectorr_getSalary(List_get(Lector, i))));

        json_t * courseObj = json_object();

        json_object_set_new(courseObj, "courseName", json_string(Lector_getCourseName(List_get(Lector, i))));
        json_object_set_new(courseObj, "numberSubjects", json_integer(Lector_getNumberOfSubjects(List_get(Lector, i))));

        json_object_set_new(json, "course", courseObj);

        json_array_append(arr, json);
    }
    char * jsonString = json_dumps(arr, JSON_INDENT(3));

    for(int i = 0;i<strlen(jsonString);i++){
        if(jsonString[i] == ',' && jsonString[i+1] == '\n' && jsonString[i-1] == '}'){
            jsonString[i+1] = ' ';
        }
    }

    fprintf(fp,"%s",jsonString);
    puts("\n-------Saved as:-------\n");
    puts(jsonString);
    json_decref(arr);
    fclose(fp);
    return true;
}

bool XmlLoader_saveToString(const char * fileName, List * Lector){
    FILE * fp = fopen(fileName, "w");
    xmlDoc * doc = NULL;
    xmlNode * rootNode = NULL;
    xmlNode * lectorNode = NULL;
    xmlNode * courseNode = NULL;
    if(fp == NULL){
        return false;
    }

    char str[10000];
    rootNode = xmlNewNode(NULL, BAD_CAST "lector");
    doc = xmlNewDoc(BAD_CAST "1.0");
    xmlDocSetRootElement(doc, rootNode);
    for(int i = 0; i < List_count(Lector); i++){
        lectorNode = xmlNewChild(rootNode, NULL, BAD_CAST "lector", NULL);

        sprintf(str, "%s", Lector_getName(List_get(Lector, i)));
        xmlNewChild(lectorNode, NULL, BAD_CAST "Name", BAD_CAST str);
        sprintf(str, "%s", Lector_getSurname(List_get(Lector, i)));
        xmlNewChild(lectorNode,NULL, BAD_CAST "Surname", BAD_CAST str);
        sprintf(str, "%i", Lector_getYear(List_get(Lector, i)));
        xmlNewChild(lectorNode, NULL, BAD_CAST "bornYear", BAD_CAST str);
        sprintf(str, "%.2f", Lectorr_getSalary(List_get(Lector, i)));
        xmlNewChild(lectorNode, NULL, BAD_CAST "Salary", BAD_CAST str);

        courseNode = xmlNewChild(lectorNode, NULL, BAD_CAST "course", NULL);
        sprintf(str, "%s", Lector_getCourseName(List_get(Lector, i)));
        xmlNewChild(courseNode, NULL, BAD_CAST "courseName", BAD_CAST str);
        sprintf(str, "%i", Lector_getNumberOfSubjects(List_get(Lector, i)));
        xmlNewChild(courseNode, NULL, BAD_CAST "numberSubjects", BAD_CAST str);

        xmlBuffer * bufferPtr = xmlBufferCreate();
        xmlNodeDump(bufferPtr, NULL, (xmlNode *)doc, 0, 1);
        sprintf(str, "%s", (const char *)bufferPtr->content);
    }
    puts("\n-------Saved as:-------\n");
    fprintf(fp , "%s" , str);
    puts(str);
    fclose(fp);
    xmlFreeDoc(doc);
    return true;
}

List* Storage_readAsXml(const char * fileName) {
    char xmlStr[1500] = "";
    readAllText(fileName, xmlStr);

    xmlDoc * xDoc = xmlReadMemory(xmlStr, (int)strlen(xmlStr), NULL, NULL, 0);
    if (NULL == xDoc) {
        printf("Error parsing xml");
        return NULL;
    }

    List * list = List_new();
    xmlNode * xRootEl = xmlDocGetRootElement(xDoc);
    for(xmlNode * xCur = xRootEl->children; NULL != xCur ; xCur = xCur->next) {
        if (XML_ELEMENT_NODE == xCur->type) {
            Lector * self = Lector_new("", "", 0, 0.0, "", 0);
            for (xmlNode * xJ = xCur->children; NULL != xJ ; xJ = xJ->next) {
                if (XML_ELEMENT_NODE == xJ->type) {
                    char * content = (char *)xmlNodeGetContent(xJ);
                    if(xmlStrcmp(xJ->name, BAD_CAST "Name") == 0) {
                        Lector_setName(self, content);
                    } else if(xmlStrcmp(xJ->name, BAD_CAST "Surname") == 0) {
                        Lector_setSurname(self,content);
                    } else if(xmlStrcmp(xJ->name, BAD_CAST "bornYear") == 0) {
                        Lector_setBornYear(self,atoi(content));
                    } else if(xmlStrcmp(xJ->name, BAD_CAST "Salary") == 0) {
                        Lector_setSalary(self, (float)atof(content));
                    } else if(xmlStrcmp(xJ->name, BAD_CAST "course") == 0) {
                        for(xmlNode* xS = xJ->children; NULL != xS; xS = xS->next) {
                            if (XML_ELEMENT_NODE == xS->type) {
                                char* source = (char*)xmlNodeGetContent(xS);
                                if(xmlStrcmp(xS->name, BAD_CAST "courseName") == 0) {
                                    Lector_setCourseName(self,source);
                                } else if(xmlStrcmp(xS->name, BAD_CAST "numberSubjects") == 0) {
                                    Lector_setNumberOfSubjects(self, atoi(source));
                                }
                                xmlFree(source);
                            }

                        }
                    }
                    xmlFree(content);
                }
            }
            List_add(list, self);
        }
    }
    xmlFreeDoc(xDoc);
    return list;
}

List * Storage_readAsJson(const char * fileName) {
    char jsonStr[1000] = "";
    readAllText(fileName, jsonStr);

    List * list = List_new();

    json_error_t err;
    json_t * jsonArr = json_loads(jsonStr, 0, &err);
    int index = 0;
    json_t * value = NULL;
    json_array_foreach(jsonArr, index, value) {

        json_t * courseObj = json_object_get(value, "course");
        Lector * self = Lector_new("", "", 0, 0.0, "", 0);

        Lector_setName(self,json_string_value(json_object_get(value, "Name")));
        Lector_setSurname(self,json_string_value(json_object_get(value, "Surname")));
        Lector_setBornYear(self,(int)json_integer_value(json_object_get(value, "bornYear")));
        Lector_setSalary(self,(float)json_real_value(json_object_get(value, "Salary")));
        Lector_setCourseName(self,json_string_value(json_object_get(courseObj, "courseName")));
        Lector_setNumberOfSubjects(self,(int)json_integer_value(json_object_get(courseObj, "numberSubjects")));

        List_add(list, self);
    }
    json_decref(jsonArr);
    return list;
}
