#ifndef ARRAY_H
#define ARRAY_H

#include <stdlib.h>


typedef struct Array Array;

struct Array {
    size_t itemSize;
    char * items;
    size_t length;
};

void Array_copy(
    Array sourceArray,
    int sourceIndex,
    Array destinationArray,
    int destinationIndex,
    int length
);

#endif