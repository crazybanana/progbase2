#ifndef CONVERT_H
#define CONVERT_H

typedef struct Writer Writer;
typedef struct Book Book;

void Writer_free(Writer ** selfPtr);

void Writer_print(Writer * self);

char * Writer_toString(Writer * self, char * buffer);

Writer * Writer_newFromString(char * str);

int Writer_getYear(Writer * self);

char * Writer_getName(Writer * self);

char * Writer_getSurname(Writer * self);

float Writer_getDate(Writer * self);

char * Writer_getBookN(Writer * self);

int Writer_getBookY(Writer * self);

void Writer_changeField(Writer * self, char * field, void * value);

int Writer_setField(Writer * self, const char* field, const char* data);

void Writer_setDate(Writer * self, float data);

void Writer_setBookY(Writer * self, int data);

void Writer_setYear(Writer * self, int data);

void Writer_setSurname(Writer * self, char * data);

void Writer_setName(Writer * self, char * data);

void Writer_setBookN(Writer * self, char * data);

Writer * Writer_new(
	char * name, 
	char * surname, 
	int bornYear, 
	float bornDM, 
	char * bookName, 
	int pubYear
	);
	
#endif