#ifndef LIST_H
#define LIST_H

#include <stdlib.h>
#include <stdbool.h>

typedef struct List List;

List * List_new(void);

void List_free(List ** selfPtr);

void * List_get(List * self, int index);

void List_set(List * self, int index, void * ref);

void List_add(List * self, void * ref);

void List_copyTo(List * self, void * array, int arrayIndex);

void List_clear(List * self);

void List_insert(List * self, int index, void * ref);

void List_removeAt(List * self, int index);

int List_indexOf(List * self, void * ref);

int  List_count(List * self);

bool List_contains(List * self, void * ref);

bool List_isEmpty(List * self);

bool List_remove(List * self, void * ref);

#endif