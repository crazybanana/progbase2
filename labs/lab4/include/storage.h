#ifndef STORAGE_H
#define STORAGE_H

#include <list.h>

List * Storage_load(const char * fileName);
bool Storage_save(const char * fileName, List * writer);
bool readAllText(const char * fileName, char * text);
bool JsonLoader_saveToString(const char * fileName, List * writer);
bool XmlLoader_saveToString(const char * fileName, List * writer);
List * Storage_readAsJson(const char * fileName);
List* Storage_readAsXml(const char * fileName);

#endif