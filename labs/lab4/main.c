#include <progbase.h>
#include <string.h>
#include <pbconsole.h>
#include <convert.h>
#include <storage.h>
#include <cui.h>
#include <list.h>
#include <errno.h>
#include <module.h>
#include <check.h>


START_TEST (listfree_emptyList_passes)
{
	List * list = List_new();
	List_free(&list);
  	ck_assert_ptr_null(list);
}
END_TEST

START_TEST (list_changeField_passes)
{
	List * writer = List_new();
	List_add(writer, Writer_newFromString("Taras;Bulba;4;4.4;lul;5."));
	Writer_changeField(List_get(writer, 0), "Name", "check");
	Writer_changeField(List_get(writer, 0), "Surname", "check");
	Writer_changeField(List_get(writer, 0), "BornY", "4");
	Writer_changeField(List_get(writer, 0), "BornDM", "13.11");
	Writer_changeField(List_get(writer, 0), "BookN", "check");
	Writer_changeField(List_get(writer, 0), "BookY", "13");
	List_free(&writer);
}
END_TEST

START_TEST (list_setField_passes)
{
	List * writer = List_new();
	List_add(writer, Writer_newFromString("Taras;Bulba;4;4.4;lul;5."));
	Writer_setField(List_get(writer, 0), "name", "check");
	Writer_setField(List_get(writer, 0), "surname", "check");
	Writer_setField(List_get(writer, 0), "year", "4");
	Writer_setField(List_get(writer, 0), "dateAndMonth", "13.11");
	Writer_setField(List_get(writer, 0), "bookN", "check");
	Writer_setField(List_get(writer, 0), "bookY", "13");
	List_free(&writer);
}
END_TEST


START_TEST (listfree_oneItem_passes)
{
  const int testValue = 3;
	List * list = List_new();
	List_add(list, (int *)&testValue);
	List_free(&list);
}
END_TEST

START_TEST (min_oneValue_returnsThatOneValue)
{
	const int testValue = 3;
	List * list = List_new();
	List_add(list, (int *)&testValue);
	ck_assert_int_eq(Module_min(list), testValue);
  List_free(&list);
}
END_TEST

START_TEST (min_someValues_returnsMinimum)
{
	int values[] = {5, -13, 0};
	const int testValue = values[1];
	List * list = List_new();
	for (int i = 0; i < 3; i++) {
		List_add(list, &values[i]);
	}
	ck_assert_int_eq(Module_min(list), testValue);
  List_free(&list);
}
END_TEST

START_TEST (min_emptyList_errnoErrorListEmpty)
{
	List * list = List_new();
	(void)Module_min(list);
	ck_assert_int_eq(errno, MODULE_ERR_LIST_EMPTY);
  List_free(&list);
}
END_TEST

Suite *test_suite() {
  Suite *s = suite_create("Module");
  TCase *tc_sample;

  tc_sample = tcase_create("TestCase");
  tcase_add_test(tc_sample, min_emptyList_errnoErrorListEmpty);
  tcase_add_test(tc_sample, min_oneValue_returnsThatOneValue);
  tcase_add_test(tc_sample, min_someValues_returnsMinimum);

  TCase * tc_list = tcase_create("List");
  tcase_add_test(tc_list, listfree_oneItem_passes);
  tcase_add_test(tc_list, listfree_emptyList_passes);
  tcase_add_test(tc_list, list_changeField_passes);
  tcase_add_test(tc_list, list_setField_passes);

  suite_add_tcase(s, tc_sample);
  suite_add_tcase(s, tc_list);

  return s;
}

void scanfErr(){
	char scanf_err;
	while ((scanf_err = getchar()) != '\n' && scanf_err != EOF) { }
}

int main(void){
	Suite *s = test_suite();
	SRunner *sr = srunner_create(s);
	srunner_set_fork_status(sr, CK_NOFORK);

	srunner_run_all(sr, CK_VERBOSE);

	srunner_free(sr);

	char list_buffer[50];
	char kek[50];
	int smth = 0;
	float smthF = 0;
	int choose;
	int howMany;
	List * writer = List_new(); 
	conClear();
	main_menu();
	scanf("%i", &choose);
	scanfErr();
	conClear();
	int on = 0;
	switch(choose){
		case 1:
			main_menu1();
			scanf("%i", &howMany);
			scanfErr();
			printf("Type: Name;Surname;BornY;BornDM;BookN;BookY.\n");	
			for(int i = 0; i < howMany; i++){
				printf("%i)", i+1);
				scanf("%s", list_buffer);
				scanfErr();
				List_add(writer, Writer_newFromString(list_buffer));
				memset(list_buffer, 0, sizeof(list_buffer));
			}
			conClear();
			for (int i = 0; i < List_count(writer); i++) {
				Writer_print(List_get(writer, i));
			}
			puts("");
			while(on != 1){
				second_menu1();
				scanf("%i", &choose);
				scanfErr();
				switch(choose){
					case 1:
						conClear();
						for (int i = 0; i < List_count(writer); i++) {
							Writer_print(List_get(writer, i));
						}
						puts("");
						second_menu11();
						scanf("%s", list_buffer);
						scanfErr();
						conClear();
						List_add(writer, Writer_newFromString(list_buffer));
						for (int i = 0; i < List_count(writer); i++) {
							Writer_print(List_get(writer, i));
						}
						puts("");
						break;
					case 2:
						conClear();
						for (int i = 0; i < List_count(writer); i++) {
							Writer_print(List_get(writer, i));
						}
						puts("");
						second_menu12();
						scanf("%i", &howMany);
						scanfErr();
						conClear();
						List_removeAt(writer, howMany-1);
						for (int i = 0; i < List_count(writer); i++) {
							Writer_print(List_get(writer, i));
						}
						puts("");
						break;
					case 3:
						conClear();
						for (int i = 0; i < List_count(writer); i++) {
							Writer_print(List_get(writer, i));
						}
						puts("");
						second_menu13();
						scanf("%i", &howMany);
						scanfErr();
						printf("What field do you want to change?\n");
						printf("Input: ");
						scanf("%s", list_buffer);
						scanfErr();
						if(strcmp(list_buffer, "Name") == 0){
							printf("For what do you want to change?\n");
							printf("Input: ");
							scanf("%s", kek);
							scanfErr();
							Writer_changeField(List_get(writer, howMany-1), list_buffer, kek);
						}else if(strcmp(list_buffer, "Surname") == 0){
							printf("For what do you want to change?\n");
							printf("Input: ");
							scanf("%s", kek);
							scanfErr();
							Writer_changeField(List_get(writer, howMany-1), list_buffer, kek);
						}else if(strcmp(list_buffer, "BookN") == 0){
							printf("For what do you want to change?\n");
							printf("Input: ");
							scanf("%s", kek);
							scanfErr();
							Writer_changeField(List_get(writer, howMany-1), list_buffer, kek);
						}else if(strcmp(list_buffer, "BornY") == 0){
							printf("For what do you want to change?\n");
							printf("Input: ");
							scanf("%i", &smth);
							scanfErr();
							Writer_changeField(List_get(writer, howMany-1), list_buffer, &smth);
						}else if(strcmp(list_buffer, "BornDM") == 0){
							printf("For what do you want to change?\n");
							printf("Input: ");
							scanf("%f", &smthF);
							scanfErr();
							Writer_changeField(List_get(writer, howMany-1), list_buffer, &smthF);
						}else if(strcmp(list_buffer, "BookY") == 0){
							printf("For what do you want to change?\n");
							printf("Input: ");
							scanf("%i", &smth);
							scanfErr();
							Writer_changeField(List_get(writer, howMany-1), list_buffer, &smth);
						}
						conClear();
						for (int i = 0; i < List_count(writer); i++) {
							Writer_print(List_get(writer, i));
						}
						puts("");
						break;
					case 4:
						conClear();
						for (int i = 0; i < List_count(writer); i++) {
							Writer_print(List_get(writer, i));
						}
						puts("");
						second_menu14();
						scanf("%i", &howMany);
						scanfErr();
						conClear();
						for(int i = 0; i < List_count(writer); i++){
							if(Writer_getYear(List_get(writer, i)) < howMany){
								Writer_print(List_get(writer, i));
							}
						}
						puts("");
						break;
					case 5:
						conClear();
						for (int i = 0; i < List_count(writer); i++) {
							Writer_print(List_get(writer, i));
						}
						second_menu25();
						scanf("%s", kek);
						scanfErr();
						conClear();
						for (int i = 0; i < List_count(writer); i++) {
							Writer_print(List_get(writer, i));
						}	
						second_menu25s();
						scanf("%i", &howMany);
						scanfErr();
						switch(howMany){
							case 1:
								conClear();
								strcat(kek, ".txt");
								Storage_save(kek, writer);
								break;
							case 2:
								conClear();
								strcat(kek, ".xml");
								XmlLoader_saveToString(kek, writer);
								break;
							case 3:
								conClear();
								strcat(kek, ".xml");
								JsonLoader_saveToString(kek, writer);
								break;
						}
						break;
					case 6:
						return main();
						break;
					default:
						conClear();
						printf("\tNo such number in menu\n\n");
						break;
				}
			}
			break;
		case 2:
			main_menu2();
			scanf("%s", kek);
			scanfErr();
			conClear();
			if(strstr(kek, ".txt")){
				writer = Storage_load(kek);
				for (int i = 0; i < List_count(writer); i++) {
					Writer_print(List_get(writer, i));
				}	
			}
			if(strstr(kek, ".xml")){
				writer = Storage_readAsXml(kek);
				for (int i = 0; i < List_count(writer); i++) {
					Writer_print(List_get(writer, i));
				}	
			}
			if(strstr(kek, ".json")){
				writer = Storage_readAsJson(kek); 
				for (int i = 0; i < List_count(writer); i++) {
					Writer_print(List_get(writer, i));
				}	
			}
			while(on != 1){
				second_menu2();
				scanf("%i", &howMany);
				switch(howMany){
					case 1:
						conClear();
						for (int i = 0; i < List_count(writer); i++) {
							Writer_print(List_get(writer, i));
						}
						puts("");
						second_menu21();
						scanf("%s", list_buffer);
						scanfErr();
						conClear();
						List_add(writer, Writer_newFromString(list_buffer));
						for (int i = 0; i < List_count(writer); i++) {
							Writer_print(List_get(writer, i));
						}
						puts("");
						break;
					case 2:
						conClear();
						for (int i = 0; i < List_count(writer); i++) {
							Writer_print(List_get(writer, i));
						}
						puts("");
						second_menu22();
						scanf("%i", &howMany);
						scanfErr();
						conClear();
						List_removeAt(writer, howMany-1);
						for (int i = 0; i < List_count(writer); i++) {
							Writer_print(List_get(writer, i));
						}
						puts("");
						break;
					case 3:
						conClear();
						for (int i = 0; i < List_count(writer); i++) {
							Writer_print(List_get(writer, i));
						}
						puts("");
						second_menu23();
						scanf("%i", &howMany);
						scanfErr();
						printf("What field do you want to change?\n");
						printf("Input: ");
						scanf("%s", list_buffer);
						scanfErr();
						if(strcmp(list_buffer, "Name") == 0){
							printf("For what do you want to change?\n");
							printf("Input: ");
							scanf("%s", kek);
							scanfErr();
							Writer_changeField(List_get(writer, howMany-1), list_buffer, kek);
						}else if(strcmp(list_buffer, "Surname") == 0){
							printf("For what do you want to change?\n");
							printf("Input: ");
							scanf("%s", kek);
							scanfErr();
							Writer_changeField(List_get(writer, howMany-1), list_buffer, kek);
						}else if(strcmp(list_buffer, "BookN") == 0){
							printf("For what do you want to change?\n");
							printf("Input: ");
							scanf("%s", kek);
							scanfErr();
							Writer_changeField(List_get(writer, howMany-1), list_buffer, kek);
						}else if(strcmp(list_buffer, "BornY") == 0){
							printf("For what do you want to change?\n");
							printf("Input: ");
							scanf("%i", &smth);
							scanfErr();
							Writer_changeField(List_get(writer, howMany-1), list_buffer, &smth);
						}else if(strcmp(list_buffer, "BornDM") == 0){
							printf("For what do you want to change?\n");
							printf("Input: ");
							scanf("%f", &smthF);
							scanfErr();
							Writer_changeField(List_get(writer, howMany-1), list_buffer, &smthF);
						}else if(strcmp(list_buffer, "BookY") == 0){
							printf("For what do you want to change?\n");
							printf("Input: ");
							scanf("%i", &smth);
							scanfErr();
							Writer_changeField(List_get(writer, howMany-1), list_buffer, &smth);
						}
						conClear();
						for (int i = 0; i < List_count(writer); i++) {
							Writer_print(List_get(writer, i));
						}
						puts("");
						break;
					case 4:
						conClear();
						for (int i = 0; i < List_count(writer); i++) {
							Writer_print(List_get(writer, i));
						}
						puts("");
						second_menu24();
						scanf("%i", &howMany);
						scanfErr();
						conClear();
						for(int i = 0; i < List_count(writer); i++){
							if(Writer_getYear(List_get(writer, i)) < howMany){
								Writer_print(List_get(writer, i));
							}
						}
						puts("");
						break;
					case 5:
						conClear();
						for (int i = 0; i < List_count(writer); i++) {
							Writer_print(List_get(writer, i));
						}
						second_menu25();
						scanf("%s", kek);
						scanfErr();
						conClear();
						for (int i = 0; i < List_count(writer); i++) {
							Writer_print(List_get(writer, i));
						}	
						second_menu25s();
						scanf("%i", &howMany);
						scanfErr();
						switch(howMany){
							case 1:
								conClear();
								strcat(kek, ".txt");
								Storage_save(kek, writer);
								break;
							case 2:
								conClear();
								strcat(kek, ".xml");
								XmlLoader_saveToString(kek, writer);
								break;
							case 3:
								conClear();
								strcat(kek, ".json");
								JsonLoader_saveToString(kek, writer);
								break;
						}
						break;
					case 6:
						return main();
						break;
					default:
						conClear();
						printf("\tNo such number in menu\n\n");
						break;
				}
			}
			break;
		case 3:
			for (int i = 0; i < List_count(writer); i++) {
				Writer * writers = (Writer *)List_get(writer, i);
				Writer_free(&writers);
			}
			List_free(&writer);
			break;
		default:
			return main();
			break;
	}
	return 0;
}