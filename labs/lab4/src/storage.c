#include <storage.h>
#include <list.h>
#include <convert.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include <jansson.h>
#include <libxml/parser.h>
#include <libxml/tree.h>

#define throw(MSG) assert(0 && MSG);

List * Storage_load(const char * fileName){
	List * list = List_new();

	char buffer[1000] = "";
	readAllText(fileName, buffer);
	
	char * str = strtok(buffer, "\n\0");

	while(str != NULL){
		List_add(list, Writer_newFromString(str));
		str = strtok (NULL, "\n\0");
	}
	return list;
}

bool readAllText(const char * fileName, char * text){
	char line[1000];
	FILE * fr = fopen(fileName, "r");
	if(fr == NULL) return false;
	while(fgets(line,100,fr)){
		strcat(text, line);
	}
	fclose(fr);
	return true;
}

bool Storage_save(const char * fileName, List * writer){
	FILE * fp = fopen(fileName, "w");
	if(fp == NULL){
		return false;
	}
	
	printf("List saved to %s\n", fileName);
	char buffer[100];
	for (int i = 0; i < List_count(writer); i++) {
		Writer * c = (Writer *)List_get(writer, i);
		char * strPtr = Writer_toString(c, buffer);
		printf("\t%i) %s\n", i+1, strPtr);
		fprintf(fp, "%s", strPtr);
	}
	fclose(fp);
	return true;
}

bool JsonLoader_saveToString(const char * fileName, List * writer){
	FILE * fp = fopen(fileName, "w");
	if(fp == NULL){
		return false;
	}
	
	for(int i = 0; i < List_count(writer); i++){
		json_t * json = json_object();
		json_object_set_new(json, "name", json_string(Writer_getName(List_get(writer, i))));
		json_object_set_new(json, "surname", json_string(Writer_getSurname(List_get(writer, i))));
		json_object_set_new(json, "year", json_integer(Writer_getYear(List_get(writer, i))));
		json_object_set_new(json, "dateAndMonth", json_real(Writer_getDate(List_get(writer, i))));
		json_t * writerObj = json_object();
		json_object_set_new(writerObj, "bookN", json_string(Writer_getBookN(List_get(writer, i))));
		json_object_set_new(writerObj, "bookY", json_integer(Writer_getBookY(List_get(writer, i))));
		json_object_set_new(json, "book", writerObj);

		char * jsonString = json_dumps(json, JSON_INDENT(2));
		fputs(jsonString, fp);
		puts(jsonString);
		free(jsonString);

		json_decref(json);
	}
	free(fp);
	return true;
}

bool XmlLoader_saveToString(const char * fileName, List * writer){
	FILE * fp = fopen(fileName, "w");
	if(fp == NULL){
		return false;
	}
	char str[1000];
	for(int i = 0; i < List_count(writer); i++){
		xmlDoc * doc = NULL;
		xmlNode * rootNode = NULL;
		xmlNode * BookNode = NULL;
		xmlNode * writerNode = NULL;

		doc = xmlNewDoc(BAD_CAST "1.0");
		
		rootNode = xmlNewNode(NULL, BAD_CAST "writer");
		xmlDocSetRootElement(doc, rootNode);

		BookNode = xmlNewChild(rootNode, NULL, BAD_CAST "writer", NULL);
		sprintf(str, "%s", Writer_getName(List_get(writer, i)));
		xmlNewProp(BookNode, BAD_CAST "name", BAD_CAST str);
		sprintf(str, "%s", Writer_getSurname(List_get(writer, i)));
		xmlNewProp(BookNode, BAD_CAST "surname", BAD_CAST str);
		sprintf(str, "%i", Writer_getYear(List_get(writer, i)));
		xmlNewChild(BookNode, NULL, BAD_CAST "year", BAD_CAST str);
		sprintf(str, "%.2f", Writer_getDate(List_get(writer, i))); 
		xmlNewChild(BookNode, NULL, BAD_CAST "dateAndMonth", BAD_CAST str);

		writerNode = xmlNewChild(BookNode, NULL, BAD_CAST "book", NULL);
		sprintf(str, "%s", Writer_getBookN(List_get(writer, i)));
		xmlNewProp(writerNode, BAD_CAST "bookN", BAD_CAST str);
		sprintf(str, "%i", Writer_getBookY(List_get(writer, i)));
		xmlNewChild(writerNode, NULL, BAD_CAST "bookY", BAD_CAST str);

		xmlBuffer * bufferPtr = xmlBufferCreate();
		xmlNodeDump(bufferPtr, NULL, (xmlNode *)doc, 0, 1);
		sprintf(str, "%s", (const char *)bufferPtr->content);
		fputs(str, fp);
		xmlFreeDoc(doc);
	}
	free(fp);
	return true;
}

List* Storage_readAsXml(const char * fileName) {
	char xmlStr[1500] = "";
	readAllText(fileName, xmlStr);

	xmlDoc * xDoc = xmlReadMemory(xmlStr, strlen(xmlStr), NULL, NULL, 0);
    if (NULL == xDoc) {
        printf("Error parsing xml");
        return NULL;
	}

	List* list = List_new();
	xmlNode * xRootEl = xmlDocGetRootElement(xDoc);
	for(xmlNode * xCur = xRootEl->children; NULL != xCur ; xCur = xCur->next) { 
        if (XML_ELEMENT_NODE == xCur->type) {
			Writer* self = Writer_new("", "", 0, 0.0, "", 0);
             for (xmlNode * xJ = xCur->children; NULL != xJ ; xJ = xJ->next) {
                if (XML_ELEMENT_NODE == xJ->type) {
                    char * content = (char *)xmlNodeGetContent(xJ);
                    if(xmlStrcmp(xJ->name, BAD_CAST "name") == 0) {
                        Writer_setField(self,"name", (char*) content);
                    } else if(xmlStrcmp(xJ->name, BAD_CAST "surname") == 0) {
                        Writer_setField(self,"surname", (char*) content);
                    } else if(xmlStrcmp(xJ->name, BAD_CAST "year") == 0) {
                        Writer_setField(self,"year", (char*)content);
					} else if(xmlStrcmp(xJ->name, BAD_CAST "dateAndMonth") == 0) {
						Writer_setField(self, "dateAndMonth", (char*)content);
                    } else if(xmlStrcmp(xJ->name, BAD_CAST "book") == 0) {
						for(xmlNode* xS = xJ->children; NULL != xS; xS = xS->next) {
							if (XML_ELEMENT_NODE == xS->type) {
								char* source = (char*)xmlNodeGetContent(xS);
								if(xmlStrcmp(xS->name, BAD_CAST "bookN") == 0) {
									Writer_setField(self, "bookN", (char*)source);
								} else if(xmlStrcmp(xS->name, BAD_CAST "bookY") == 0) {
									Writer_setField(self, "bookY", (char*)source);
								}
								xmlFree(source);
							}
                        
                    	}
					}

                    xmlFree(content);
                }
             }
			 List_add(list, self);
        }
    }
    xmlFreeDoc(xDoc);
	return list;
}

List * Storage_readAsJson(const char * fileName) {
	char jsonStr[1000] = "";
	readAllText(fileName, jsonStr);

	List * list = List_new();

	json_error_t err;
	json_t * jsonArr = json_loads(jsonStr, 0, &err);
	int index = 0;
	json_t * value = NULL;
	json_array_foreach(jsonArr, index, value) {
		json_t * bookObj = json_object_get(value, "book");
		Writer * self = Writer_new("", "", 0, 0.0, "", 0);
		Writer_setName(self, (char *)json_string_value(json_object_get(value, "name")));
		Writer_setSurname(self, (char *)json_string_value(json_object_get(value, "surname")));
		Writer_setYear(self, json_integer_value(json_object_get(value, "year")));
		Writer_setDate(self, json_real_value(json_object_get(value, "dateAndMonth")));
		Writer_setBookN(self, (char *)json_string_value(json_object_get(bookObj, "bookN")));
		Writer_setBookY(self, json_integer_value(json_object_get(bookObj, "bookY")));
		List_add(list, self);
	}
	json_decref(jsonArr);
	return list;
}
