#include <cui.h>
#include <stdlib.h>
#include <stdio.h>
#include <progbase.h>
#include <progbase/console.h>

void main_menu(void){
	printf("\tMenu\n");
	printf("1.Create new list\n");
	printf("2.Create new list from file\n");
	printf("3.Exit\n");
	printf("Input: ");
}

void main_menu1(void){
	printf("\tMenu\n");
	printf("1.<Create new list>\n");
	printf("2.Create new list from file\n");
	printf("3.Exit\n");
	printf("How many writers do you want to create?\n");		
	printf("Input: ");
}

void main_menu2(void){
	printf("\tMenu\n");
	printf("1.Create new list\n");
	printf("2.<Create new list from file>\n");
	printf("Input file name\n");
	printf("Input: ");
}

void second_menu1(void){
	printf("\t(Create new list)\n");
	printf("1.Add new writer\n");
	printf("2.Delete the writer\n");
	printf("3.Change the chosen writer\n");
	printf("4.Find all writers that borned before chosen year\n");
	printf("5.Save your current list to file\n");
	printf("6.Return to main menu\n");
	printf("Input: ");
}

void second_menu11(void){
	printf("\t(Create new list)\n");
	printf("1.<Add new writer>\n");
	printf("2.Delete the writer\n");
	printf("3.Change the chosen writer\n");
	printf("4.Find all writers that borned before chosen year\n");
	printf("5.Save your current list to file\n");
	printf("6.Return to main menu\n");
	printf("Type: Name;Surname;BornY;BornDM;BookN;BookY.\n");	
	printf("Input: ");
}

void second_menu12(void){
	printf("\t(Create new list)\n");
	printf("1.Add new writer\n");
	printf("2.<Delete the writer>\n");
	printf("3.Change the chosen writer\n");
	printf("4.Find all writers that borned before chosen year\n");
	printf("5.Save your current list to file\n");
	printf("6.Return to main menu\n");
	printf("Writer index that do you want to delete?\n");	
	printf("Input: ");
}

void second_menu13(void){
	printf("\t(Create new list)\n");
	printf("1.Add new writer\n");
	printf("2.Delete the writer\n");
	printf("3.<Change the chosen writer>\n");
	printf("4.Find all writers that borned before chosen year\n");
	printf("5.Save your current list to file\n");
	printf("6.Return to main menu\n");
	printf("Writer index that do you want to change\n");	
	printf("Input: ");
}

void second_menu14(void){
	printf("\t(Create new list)\n");
	printf("1.Add new writer\n");
	printf("2.Delete the writer\n");
	printf("3.Change the chosen writer\n");
	printf("4.<Find all writers that borned before chosen year>\n");
	printf("5.Save your current list to file\n");
	printf("6.Return to main menu\n");
	printf("Input year\n");	
	printf("Input: ");
}

void second_menu15(void){
	printf("\t(Create new list)\n");
	printf("1.Add new writer\n");
	printf("2.Delete the writer\n");
	printf("3.Change the chosen writer\n");
	printf("4.Find all writers that borned before chosen year\n");
	printf("5.<Save your current list to file>\n");
	printf("6.Return to main menu\n");
	printf("Choose format of file:\n1.txt\n2.xml\n3.json\n");
	printf("Input: ");
}

void second_menu15s(void){
	printf("\t(Create new list)\n");
	printf("1.Add new writer\n");
	printf("2.Delete the writer\n");
	printf("3.Change the chosen writer\n");
	printf("4.Find all writers that borned before chosen year\n");
	printf("5.<Save your current list to file>\n");
	printf("6.Return to main menu\n");
	printf("Enter file name\n");
	printf("Input: ");
}

void second_menu2(void){
	printf("\t(Create new list from file)\n");
	printf("1.Add new writer\n");
	printf("2.Delete the writer\n");
	printf("3.Change the chosen writer\n");
	printf("4.Find all writers that borned before chosen year\n");
	printf("5.Save your current list to file\n");
	printf("6.Return to main menu\n");
	printf("Input: ");
}

void second_menu21(void){
	printf("\t(Create new list from file)\n");
	printf("1.<Add new writer>\n");
	printf("2.Delete the writer\n");
	printf("3.Change the chosen writer\n");
	printf("4.Find all writers that borned before chosen year\n");
	printf("5.Save your current list to file\n");
	printf("6.Return to main menu\n");
	printf("Type: Name;Surname;BornY;BornDM;BookN;BookY.\n");	
	printf("Input: ");
}

void second_menu22(void){
	printf("\t(Create new list from file)\n");
	printf("1.Add new writer\n");
	printf("2.<Delete the writer>\n");
	printf("3.Change the chosen writer\n");
	printf("4.Find all writers that borned before chosen year\n");
	printf("5.Save your current list to file\n");
	printf("6.Return to main menu\n");
	printf("Writer index that do you want to delete?\n");	
	printf("Input: ");
}

void second_menu23(void){
	printf("\t(Create new list from file)\n");
	printf("1.Add new writer\n");
	printf("2.Delete the writer\n");
	printf("3.<Change the chosen writer>\n");
	printf("4.Find all writers that borned before chosen year\n");
	printf("5.Save your current list to file\n");
	printf("6.Return to main menu\n");
	printf("Writer index that do you want to change\n");	
	printf("Input: ");
}

void second_menu24(void){
	printf("\t(Create new list from file)\n");
	printf("1.Add new writer\n");
	printf("2.Delete the writer\n");
	printf("3.Change the chosen writer\n");
	printf("4.<Find all writers that borned before chosen year>\n");
	printf("5.Save your current list to file\n");
	printf("6.Return to main menu\n");
	printf("Input year\n");	
	printf("Input: ");
}

void second_menu25(void){
	printf("\t(Create new list from file)\n");
	printf("1.Add new writer\n");
	printf("2.Delete the writer\n");
	printf("3.Change the chosen writer\n");
	printf("4.Find all writers that borned before chosen year\n");
	printf("5.<Save your current list to file>\n");
	printf("6.Return to main menu\n");
	printf("Enter file name\n");
	printf("Input: ");
}

void second_menu25s(void){
	printf("\t(Create new list from file)\n");
	printf("1.Add new writer\n");
	printf("2.Delete the writer\n");
	printf("3.Change the chosen writer\n");
	printf("4.Find all writers that borned before chosen year\n");
	printf("5.<Save your current list to file>\n");
	printf("6.Return to main menu\n");
	printf("Choose format of file:\n1.txt\n2.xml\n3.json\n");
	printf("Input: ");
}