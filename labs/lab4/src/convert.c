#include "convert.h"
#include <stdio.h>
#include <stdlib.h>
#include <list.h>
#include <string.h>

struct Book{
	char bookName[50];
	int publicationYear;
};

struct Writer{
	char name[100];
	char surname[100];
	int bornYear;
	float bornDateMonth;
	Book book;
};

Writer * Writer_new(
	char * name, 
	char * surname, 
	int bornYear, 
	float bornDateMonth, 
	char * bookName, 
	int publicationYear
	){
Writer * self = malloc(sizeof(Writer));
	strcpy(self->name, name);
	strcpy(self->surname, surname);
	self->bornYear = bornYear;
	self->bornDateMonth = bornDateMonth;
	strcpy(self->book.bookName, bookName);
	self->book.publicationYear = publicationYear;
	return self;	
}

void Writer_free(Writer ** selfPtr){
	free(*selfPtr);
	*selfPtr = NULL;
}

void Writer_print(Writer * self){
	printf(" Name: %s\n Surname: %s\n Born year: %i\n Born day and month: %.2f\n Book name: %s\n Publication year: %i\n",
	self->name,
	self->surname,
	self->bornYear,
	self->bornDateMonth,
	self->book.bookName,
	self->book.publicationYear);
}

int Writer_getYear(Writer * self){ return self->bornYear;}

char * Writer_getName(Writer * self){ return self->name;}

int Writer_getBookY(Writer * self){ return self->book.publicationYear;}

char * Writer_getSurname(Writer * self){ return self->surname;}

float Writer_getDate(Writer * self){ return self->bornDateMonth;}

char * Writer_getBookN(Writer * self){ return self->book.bookName;}

char * Writer_toString(Writer * self, char * buffer){
	sprintf(buffer, "%s;%s;%i;%.2f;%s;%i.",
	self->name,
	self->surname,
	self->bornYear,
	self->bornDateMonth,
	self->book.bookName,
	self->book.publicationYear);
	return buffer;
}

char* strCopyFrom(char* str ,char* dest,int begin,int end){
    int j = 0;
    memset(dest,0,strlen(str));
    for(int i = begin; i < end;i ++){
        if(str[i]!='\n'){
            dest[j] = str[i];
            j++;
        }
    }
    return dest;
}

Writer * Writer_newFromString(char *str) {
    int beginPos = 0;
    int endPos = 0;

    char dest[1000] = "";
    Writer * self = Writer_new("", "", 0, 0.0, "", 0);
    int index = 0;

    while(str[endPos] != '\0'){
        if(str[endPos] == ';' || str[endPos+1] == '\0'){
            switch(index){
                case 0:
                    strCopyFrom(str,self->name,beginPos,endPos);
                    break;
                case 1:
				 	strCopyFrom(str,self->surname,beginPos+1,endPos);
                    break;
                case 2:
					self->bornYear = atoi(strCopyFrom(str,dest,beginPos+1,endPos));
                    break;
                case 3:
					self->bornDateMonth = atof(strCopyFrom(str,dest,beginPos+1,endPos));
                    break;
				case 4:
					strCopyFrom(str,self->book.bookName,beginPos+1,endPos);
					break;
				case 5:
					self->book.publicationYear = atoi(strCopyFrom(str,dest,beginPos+1,endPos+1));
					break;
                default:
                    return self;
            }
            beginPos = endPos;
            index++;
        }
        endPos++;
    }
    return self;
}

void Writer_changeField(Writer * self, char * field, void * value){
	if(strcmp(field, "Name") == 0){
		strcpy(self->name, (char*)value);
	}
	if(strcmp(field, "Surname") == 0){
		strcpy(self->surname, (char*)value);
	}
	if(strcmp(field, "BornY") == 0){
		self->bornYear = *((int*)value);
	}
	if(strcmp(field, "BornDM") == 0){
		self->bornDateMonth = *((float*)value);
	}
	if(strcmp(field, "BookN") == 0){
		strcpy(self->book.bookName, (char*)value);
	}
	if(strcmp(field, "BookY") == 0){
		self->book.publicationYear = *((int*)value);
	}
}

int Writer_setField(Writer * self, const char* field, const char* data) { 
	if(strcmp(field, "name") == 0) { 
		strcpy(self->name, data); 
	} else if(strcmp("surname", field) == 0) { 
		strcpy(self->surname, data); 
	} else if(strcmp(field, "year") == 0) { 
		self->bornYear = atoi(data);
	} else if(strcmp(field, "dateAndMonth") == 0) { 
		self->bornDateMonth = atof(data);
	} else if(strcmp(field, "bookN") == 0) { 
		strcpy(self->book.bookName, data);
	} else if(strcmp(field, "bookY") == 0){ 
		self->book.publicationYear = atoi(data);
	} else {
		return 1;
	}
	return 0; 
}

void Writer_setName(Writer * self, char * data){
	strcpy(self->name, data);
}

void Writer_setSurname(Writer * self, char * data){
	strcpy(self->surname, data);
}

void Writer_setDate(Writer * self, float data){
	self->bornDateMonth = data;
}

void Writer_setYear(Writer * self, int data){
	self->bornYear = data;
}

void Writer_setBookY(Writer * self, int data){
	self->book.publicationYear = data;
}

void Writer_setBookN(Writer * self, char * data){
	strcpy(self->book.bookName, data);
}