package lab3_class_skeleton.deque;

import lab3_class_skeleton.deque.SLNode;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class LinkedStack implements Stack {
	private SLNode top;
	private int size;
	
	private boolean check(){
		if(size < 0){
			return false;
		}
		if(size == 0){
			if(top != null)return false;
		}else if(size == 1){
			if(top == null)return false;
			if(top.next != null)return false;
		}else{
			if(top == null)return false;
			if(top.next == null)return false;
		}
		int numberOfNodes = 0;
        for (SLNode x = top; x != null && numberOfNodes <= size; x = x.next) {
            numberOfNodes++;
        }
        if (numberOfNodes != size) return false;
        return true;
	}

	@Override
	public boolean isEmpty() {
		return top == null;
	}

	@Override
	public int pop() {
		if(isEmpty()) throw new NoSuchElementException("Stack underflow");
		int data = top.data;
		top = top.next;
		size--;
		assert check();
		return data;
	}

	@Override
	public void push(int item) {
		SLNode oldtop = top;
		top = new SLNode(item);
		top.data = item;
		top.next = oldtop;
		size++;
		assert check();
	}

	@Override
	public int top() {
		if (isEmpty()) throw new NoSuchElementException("Stack underflow");
		return top.data;
	}

	@Override
	public int size() {
		return size;
	}
	
    public void display()
    {
        if (size == 0) 
        {
            System.out.print("Empty\n");
            return ;
        }
        SLNode ptr = top;
        while (ptr != null)
        {
            System.out.print(ptr.getData()+" ");
            ptr = ptr.getNext();
        }
        System.out.println();        
    }
}
