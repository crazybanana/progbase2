package lab3_class_skeleton.deque;

public class Main{
	public static void main(String[]args){
		ArrayList myList = new ArrayList();
		LinkedStack st = new LinkedStack();

		myList.add("C");
		myList.add("3");
		myList.add("A");
		myList.add("B");
		myList.add("AB");
		myList.add("F");
		myList.add("7");
		
		System.out.println("Array List:");
		for(int i = 0; i < myList.size(); i++){
			System.out.printf("%s ", myList.get(i));
		}
		
		for(int i = 0; i < myList.size(); i++){
			if(i == 0){
				int num = (Integer.parseInt(myList.get(i), 16)) + 
						  (Integer.parseInt(myList.get(i+1), 16));
				st.push(num);
			}else if(i == myList.size()-1){
				int num = (Integer.parseInt(myList.get(i-1), 16)) +
						  (Integer.parseInt(myList.get(i), 16));
				st.push(num);
			}else{
				int num = (Integer.parseInt(myList.get(i-1), 16)) +
						  (Integer.parseInt(myList.get(i), 16)) +
						  (Integer.parseInt(myList.get(i+1), 16));
				st.push(num);
			}
		}
		
		System.out.println("\n\nLinked Stack:");
		st.display();
	}
}