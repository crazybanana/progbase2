package lab22;

public class BottomUpMergeSort {

	final static int N = 20;

	public static void main(String[] args) {
		int[] arr = new int[N];
		ArrayAssistant.fillRandom(arr, 100);
		ArrayAssistant.print(arr, "Before merging");
		bottomUupMergeSort(arr);
		ArrayAssistant.print(arr, "After merging");

	}

	// Bottom-up merge sort
	public static void bottomUupMergeSort(int[] array) {
		int step = 1;
		while (step < array.length) {
			int l = 0;
			while (l < array.length - step) {
				int m = l + step - 1;
				int r = (l + 2 * step - 1) < (array.length - 1) ? (l + 2 * step - 1)
						: array.length - 1;
				merge(array, l, m, r);
				l += step * 2;

			}
			step = step * 2;
		}
	}

	/**
	 * merge two sorted part arr [l..m] and arr [m+1 ..r] into one sorted part
	 * arr [l..r]
	 * 
	 * @param arr
	 *            the array contains two sorted part
	 * @param l
	 *            start index of left sorted part arr [l..m]
	 * @param m
	 *            end index of of left sorted part arr [l..m]
	 * @param r
	 *            end index of right sorted part arr [m+1 ..r]
	 * 
	 */
	public static void merge(int arr[], int l, int m, int r) {
		int n1 = m - l + 1;
		int n2 = r - m;

		int[] leftArray = new int[n1];
		int[] rightArray = new int[n2];

		// copy from left subarray
		for (int i = 0; i < leftArray.length; i++)
			leftArray[i] = arr[l + i];
		// copy from right subarray
		for (int j = 0; j < rightArray.length; j++)
			rightArray[j] = arr[m + j + 1];

		for (int k = l, i = 0, j = 0; k <= r; k++) {

			if (i == leftArray.length) {
				arr[k] = rightArray[j++];
				continue;
			}
			if (j == rightArray.length) {
				arr[k] = leftArray[i++];
				continue;
			}

			if (leftArray[i] <= rightArray[j]) {
				arr[k] = leftArray[i++];
			} else {
				arr[k] = rightArray[j++];
			}
		}

	}

}
