package lab22;

import java.util.Arrays;

public class Merging {
	
	public static void main(String[] args) {
		int[] arr1 = new int[1000000];
		randFillArr(arr1);
		long before = System.nanoTime();
		Arrays.sort(arr1);
		long after = System.nanoTime();
		System.out.println("Несортированый массив N1: время исполнения = " + (after - before) + " нс.");
		//ArrayAssistant.print(arr1, "Array1 before merging");

		int[] arr2 = new int[1000000];
		randFillArr(arr2);
		before = System.nanoTime();
		Arrays.sort(arr2);
		after = System.nanoTime();
		System.out.println("Несортированый массив N2: время исполнения = " + (after - before) + " нс.");
		//ArrayAssistant.print(arr2, "Array2 before merging");

		
		before = System.nanoTime();
		int[] arrTwoWayMerging = twoWayMerge(arr1, arr2);
		after = System.nanoTime();
		System.out.println("Two way merge: время исполнения = " + (after - before) + " нс.");
		/*ArrayAssistant.print(arrTwoWayMerging,
				"After two-way merging arr1 and arr2");*/
		
		before = System.nanoTime();
		int[] arrAbstractInPlace = abstractInPlaceMerge(arr1, arr2);
		after = System.nanoTime();
		System.out.println("Abstract in place merge: время исполнения = " + (after - before) + " нс.");
		/*ArrayAssistant.print(arrAbstractInPlace,
				"After abstract-in-place merging arr1 and arr2");*/
	}
	
	public static void reverse(int[] array){
	    for (int i = 0; i < array.length / 2; i++) {
	        int tmp = array[i];
	        array[i] = array[array.length - i - 1];
	        array[array.length - i - 1] = tmp;
	    }
	}

	public static void randFillArr(int[] a){
		for (int i = 0; i < a.length; i++) {
			a[i] = (int) Math.round((Math.random() * 30) - 15);
		}
	}

	static int[] twoWayMerge(int[] leftArray, int[] rightArray) {
		int[] result = new int[leftArray.length + rightArray.length];
		int i = 0;
		int j = 0;
		for (int k = 0; k < result.length; k++) {
			if (j == rightArray.length) {
				result[k] = leftArray[i++];
				continue;
			}
			if (i == leftArray.length) {
				result[k] = rightArray[j++];
				continue;
			}
			if (leftArray[i] <= rightArray[j]) {
				result[k] = leftArray[i++];
			} else {
				result[k] = rightArray[j++];
			}
		}
		return result;
	}

	static int[] abstractInPlaceMerge(int[] leftArray, int[] rightArray) {
		int[] aux = new int[leftArray.length + rightArray.length];
		int[] a = new int[leftArray.length + rightArray.length];

		int i, j, k;
		for (i = 0, k = 0; i < leftArray.length; i++, k++) {
			aux[i] = leftArray[i];
		}

		for (j = rightArray.length - 1; k < aux.length; k++, j--) {
			aux[k] = rightArray[j];
		}

		i = 0;
		j = aux.length - 1;
		for (k = 0; k < aux.length; k++) {
			if (aux[j] < aux[i])
				a[k] = aux[j--];
			else
				a[k] = aux[i++];
		}
		return a;

	}

}
