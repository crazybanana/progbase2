package lab22;

import java.util.Arrays;

public class TopDownMergeSort {
	static int N = 15;

	public static void main(String[] args) {

		int[] arr = new int[N];
		ArrayAssistant.fillRandom(arr, 1000);
		ArrayAssistant.print(arr, "Before sorting");

		mergesort1(arr);
		ArrayAssistant.print(arr, "After sorting");
		System.out.println(System.lineSeparator());

		ArrayAssistant.fillRandom(arr, 1000);
		ArrayAssistant.print(arr, "Before sorting");
		arr = mergesort2(arr);
		ArrayAssistant.print(arr, "After sorting");

	}

	public static void mergesort1(int a[]) {
		mergesort(a, 0, a.length - 1);
	}

	/**
	 * // top-down merge sort of array arr [l..r]
	 * 
	 * @param a
	 *            the array to be sorted
	 * @param l
	 *            start index of sub-array to be sorted (0 < l < r)
	 * @param r
	 *            end index of sub-array to be sorted (l < r < arr.lenght)
	 */
	private static void mergesort(int a[], int l, int r) {
		if (l < r) {
			int m = (l + r) / 2;
			mergesort(a, l, m);
			mergesort(a, m + 1, r);
			merge(a, l, m, r);
		}
	}

	/**
	 * merge two sorted part arr [l..m] and arr [m+1 ..r] into one sorted part
	 * arr [l..r]
	 * 
	 * @param arr
	 *            the array contains two sorted part
	 * @param l
	 *            start index of left sorted part arr [l..m]
	 * @param m
	 *            end index of of left sorted part arr [l..m]
	 * @param r
	 *            end index of right sorted part arr [m+1 ..r]
	 * 
	 */
	public static void merge(int arr[], int l, int m, int r) {
		int n1 = m - l + 1;
		int n2 = r - m;

		int[] leftArray = new int[n1];
		int[] rightArray = new int[n2];

		// copy from left subarray
		for (int i = 0; i < leftArray.length; i++)
			leftArray[i] = arr[l + i];
		// copy from right subarray
		for (int j = 0; j < rightArray.length; j++)
			rightArray[j] = arr[m + j + 1];

		for (int k = l, i = 0, j = 0; k <= r; k++) {

			if (i == leftArray.length) {
				arr[k] = rightArray[j++];
				continue;
			}
			if (j == rightArray.length) {
				arr[k] = leftArray[i++];
				continue;
			}

			if (leftArray[i] <= rightArray[j]) {
				arr[k] = leftArray[i++];
			} else {
				arr[k] = rightArray[j++];
			}
		}

	}

	//
	/**
	 * top-down merge sort
	 * 
	 * @param a
	 *            the array to be sorted
	 * @return sorted array
	 */
	public static int[] mergesort2(int a[]) {
		if (a.length > 1) {
			// Split the array in half in two parts
			int[] b = Arrays.copyOfRange(a, 0, a.length / 2);
			int[] c = Arrays.copyOfRange(a, b.length, a.length);

			return twoWayMerge(mergesort2(b), mergesort2(c));

		} else
			return a;
	}

	/**
	 * two way merging two sorted array into one sorted
	 * 
	 * @param leftArray
	 *            the sorted array
	 * @param rightArray
	 *            the sorted array
	 * @return the sorted array
	 */
	static int[] twoWayMerge(int[] leftArray, int[] rightArray) {
		int iLeft = 0;
		int iRight = 0;
		int iResult = 0;
		int[] result = new int[leftArray.length + rightArray.length];
		while (iLeft < leftArray.length && iRight < rightArray.length) {
			if (leftArray[iLeft] <= rightArray[iRight]) {
				result[iResult++] = leftArray[iLeft++];
			} else {
				result[iResult++] = rightArray[iRight++];
			}
		}
		System.arraycopy(leftArray, iLeft, result, iResult, leftArray.length
				- iLeft);
		System.arraycopy(rightArray, iRight, result, iResult, rightArray.length
				- iRight);
		return result;
	}

}
