package lab22;

public class HeapSort {
	private static int heapSize;
	
	public static void sort(int[] a) {
		buildHeap(a);
		while (heapSize > 1) {
			swap(a, 0, heapSize - 1);
			heapSize--;
			heapify(a, 0);
		}
	}
	
	public static void main(String[] args){
		int[] array = new int[1000000];
		randFillArr(array);
		long before = System.nanoTime();
		//System.out.printf("%d\n", before);
		sort(array);
		long after = System.nanoTime();
		//System.out.printf("%d\n", after);
		System.out.println("Несортированый массив: время исполнения = " + (after - before) + " нс.");
		before = System.nanoTime();
		sort(array);
		after = System.nanoTime();
		System.out.println("Сортированый массив: время исполнения = " + (after - before) + " нс.");
		reverse(array);
		before = System.nanoTime();
		sort(array);
		after = System.nanoTime();
		System.out.println("Сортированный в обратном направлении: время исполнения = " + (after - before) + " нс.");
		//System.out.println("Время исполнения = " + ((after - before)/1000) + " с.");
	}
	
	public static void reverse(int[] array){
	    for (int i = 0; i < array.length / 2; i++) {
	        int tmp = array[i];
	        array[i] = array[array.length - i - 1];
	        array[array.length - i - 1] = tmp;
	    }
	}

	public static void randFillArr(int[] a){
		for (int i = 0; i < a.length; i++) {
			a[i] = (int) Math.round((Math.random() * 30) - 15);
		}
	}
	
	private static void buildHeap(int[] a) {
		heapSize = a.length;
		for (int i = a.length / 2; i >= 0; i--) {
			heapify(a, i);
		}
	}
	
	private static void heapify(int[] a, int i) {
		int l = left(i);
		int r = right(i);
		int largest = i;
		if (l < heapSize && a[i] < a[l]) {
			largest = l;
		} 
		if (r < heapSize && a[largest] < a[r]) {
			largest = r;
		}
		if (i != largest) {
			swap(a, i, largest);
			heapify(a, largest);
		}
	}
	
	private static int right(int i) {
		return 2 * i + 1;
	}
	
	private static int left(int i) {
		return 2 * i + 2;
	}
	
	private static void swap(int[] a, int i, int j) {
		int temp = a[i];
		a[i] = a[j];
		a[j] = temp;
	}
}
