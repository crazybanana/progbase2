/**
 * Created by dima on 26.06.17.
 */
public class BinaryTree {
    Node root;

    public void printPaths(Node node) {
        int path[] = new int[1000];
        printPathsRecur(node, path, 0);
    }

    public void printPathsRecur(Node node, int path[], int pathLen) {
        if (node == null)
            return;

        path[pathLen] = node.data;
        pathLen++;

        if (node.left == null && node.right == null)
            printArray(path, pathLen);
        else {
            printPathsRecur(node.left, path, pathLen);
            printPathsRecur(node.right, path, pathLen);
        }
    }

    public void printArray(int ints[], int len)
    {
        int i;
        for (i = 0; i < len; i++)
            System.out.print(ints[i] + " ");
        System.out.println("");
    }
}
