public class Main {

    public static void main(String[] args) {
        BinaryTree tree = new BinaryTree();

        tree.root = new Node(1);
        tree.root.left = new Node(2);
        tree.root.right = new Node(3);
        tree.root.left.left = new Node(4);
        tree.root.left.right = new Node(5);
        tree.root.right.right = new Node(7);
        tree.root.right.left = new Node(10);
        tree.root.right.left.right = new Node(11);
        tree.root.right.left.right.right= new Node(12);
        tree.root.right.left.right.right.left = new Node(14);
        tree.root.right.left.right.right.right = new Node(13);
        tree.root.right.left.right.right.right.right = new Node(16);

        tree.printPaths(tree.root);
    }
}
