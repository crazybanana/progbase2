package task1;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;
import java.sql.Array;
import java.util.Iterator;
import java.util.List;

import com.opencsv.CSVReader;
import common.FigureSet;
import common.GeomFigure;

public class Task1Main {
	// name of file which stores data about geometric figures
	private static String fileName = "figures.csv";

	// path to the current directory that contains directory "data"
	private static String currenDir = System.getProperty("user.dir")
			+ File.separatorChar + "data";

	public static void main(String[] args) {
		// 1) read all lines from a file

		List<String[]> lines = readcsvFile(fileName, currenDir);

		if (!lines.isEmpty()) {
			int numLines = lines.size();
			System.out.println("File contains " + numLines + "  lines");

			// 2) create the array of geometric figures and check data from the
			// file
			GeomFigure[] farray = createArrayOfFigures(lines);

			// 3) Create a set of figures
			FigureSet figures = new Hastable();
			for (GeomFigure f : farray) {
				figures.add(f);
			}

			// 4) Print a set of figures
			figures.print();

		} else {
			System.out.println("Error: file  " + fileName + "   is empty");
			System.exit(0);
		}

	}

	private static List<String[]> readcsvFile(String fileName, String dirName) {
		CSVReader reader = null;
		GeomFigure[] figures = null;
		String path = Paths.get(currenDir, fileName).toString();
		List<String[]> list = null;

		// read data from a file
		try {

			reader = new CSVReader(new FileReader(path));
			System.out.println("File \"" + path + " \"  have been reading ");

			// read all lines from a file
			list = reader.readAll();
			reader.close();

		} catch (FileNotFoundException e) {
			System.out.println("Error: file  " + path + "   not found");
		} catch (IOException e) {
			System.err.println("Error:read file " + path + "   error");
		} catch (SecurityException e) {
			System.err.println(e.getMessage());
		}

		return list;
	}

	private static GeomFigure[] createArrayOfFigures(List<String[]> list) {
		int num = 0;

		// create empty array of geometric figures with the length = list.size()
		GeomFigure[] farray = new GeomFigure[list.size()];

		// start reading and analyzing each line from this list
		for (Iterator<String[]> it = list.iterator(); it.hasNext();) {

			// line contains data about one geometric figures
			String[] line = it.next();

			try {
				if (isCorrectData(line) && line.length == 4) {
					double x1 = Double.valueOf(line[0]);
					double y1 = Double.valueOf(line[1]);
					double x2 = Double.valueOf(line[2]);
					double y2 = Double.valueOf(line[3]);

					farray[num++] = new GeomFigure(x1, y1, x2, y2);
				}
			} catch (NumberFormatException e) {}

		}
		// check if all data in a file are valid
		if (num != list.size()) {

			// if not, create new array with smaller length
			farray = copyOf(farray, num);
		}

		return farray;
	}

	private static GeomFigure[] copyOf(GeomFigure[] farray, int num) {
		// Create new array of geometric figures of size num
		// Copy num first elements from farray students to the new array
		// Return new array
		GeomFigure[] FigureArray = new GeomFigure[num];
		System.arraycopy(farray, 0, FigureArray, 0, num);
		return FigureArray;
	}
	private static boolean isCorrectData(String[] lines) {
		for (int index = 0; index < lines.length; index++) {
			String str = lines[index].trim();
			if (str.equals("") || str.indexOf('.') != str.lastIndexOf('.')) return false;
			int i = 0;
			if ("+-".indexOf(str.charAt(0)) != -1) i++;
			for (; i < str.length(); i++)
				if (!Character.isDigit(str.charAt(i)) && str.charAt(i) != '.') return false;
		}
		return true;
	}
}

