package task1;

import common.FigureSet;
import common.GeomFigure;

/**
 * 
 * This class implements a set as a hash table. Hash table is an associative
 * array of entries. <br>
 * Each entry contains geometric figure or null. <br>
 * Hash table does not resolve any collisions
 *
 */
public class Hastable implements FigureSet {

	private GeomFigure[] table;

	/**
	 * the number of non-null entries in the hashtable
	 */
	private int size;

	/**
	 * default size of the hashtable
	 */
	private final static int initialCapacity = 11;

	/**
	 * The load factor is a measure of how full the hash table is allowed to get
	 * / before its capacity is automatically increased
	 */
	private static double loadFactor = 0.75;

	/**
	 * It is used in multiplication hash function
	 */
	private final double A = (Math.sqrt(5) - 1) / 2;

	/**
	 * Constructs a new, empty hashtable with a default initial capacity (11)
	 * and load factor (0.75).
	 */
	Hastable() {
		this(initialCapacity, loadFactor);
	}

	/**
	 * Constructs a new, empty hashtable with the specified initial capacity and
	 * default load factor (0.75).
	 * 
	 * @param initialCapacity
	 *            the initial capacity of the hashtable
	 */
	Hastable(int initialCapacity) {
		this(initialCapacity, loadFactor);
	}

	/**
	 * Constructs a new, empty hashtable with the specified initial capacity and
	 * the specified load factor.
	 * 
	 * @param initialCapacity
	 *            the initial capacity of the hashtable
	 * @param loadFactor
	 *            the load factor of the hashtable
	 */
	Hastable(int initialCapacity, double loadFactor) {
		if ((initialCapacity <= 0) || (loadFactor <= 0.0)) {
			throw new IllegalArgumentException();
		}
		table = new GeomFigure[initialCapacity];
		Hastable.loadFactor = loadFactor;
	}

	/**
	 * Returns the number of entries in the hashtable
	 * 
	 * @return the number of entries in the hashtable
	 */
	public int size() {
		return size;
	}

	/**
	 * Increases the capacity of and internally reorganizes this hashtable, in
	 * order to accommodate and access its entries more efficiently. This method
	 * is called when the number of elements in the hashtable exceeds this
	 * hashtable's capacity and load factor
	 */
	private void rehash() {
		GeomFigure[] oldTable = table;
		table = new GeomFigure[table.length + 16];
		for (GeomFigure tmp :oldTable)
			if (add(tmp)) size--;
	}

	/**
	 * The hash function is used to calculate the hasvalue of the object gf.
	 * Choose hashing method from your variant (table 1): deletion or
	 * multiplication
	 * 
	 * @param gf
	 *            some geometric figure
	 * @return hash value - index in table
	 */
	private int hash(GeomFigure gf) {
		if (gf == null) throw new NullPointerException();
		return (int)(table.length * (gf.hashCode() * A - (int)(gf.hashCode() * A)));
	}

	@Override
	public boolean add(GeomFigure gf) {
		// if gf is not null
		// ---calculate number of slot where should be object gf
		// ------if slot is empty
		// ----------write gf to the slot and rehash if need
		// ----------increase the size of hash table
		if (gf == null) return false;
		int index = hash(gf);
		if (table[index] != null) return false;
		table[index] = gf;
		size++;
		if (size >= (int)(table.length * loadFactor)) rehash();
		return true;
	}

	@Override
	public boolean contains(GeomFigure gf) {
		// if gf is not null
		// ----Calculate hashvalue of gf
		// ------if slot is not empty
		// ---------compare gf and object from the slot
		return gf != null && gf.equals(table[hash(gf)]);
	}

	@Override
	public boolean remove(GeomFigure gf) {
		// if gf is not null, return false
		// ---Calculate number of slot where should be object gf
		// ---if slot is not empty
		// -------write null into the slot
		// -------decrease the size of hash table
		// -------return true
		if (!contains(gf)) return false;
		table[hash(gf)] = null;
		size--;
		return true;
	}

	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	@Override
	public void print() {
		// Output the table , where each row contains a number of the hash-table
		// slot, and the element itself.For example,

		// System.out.println(String.format(" %-6d|%s", index, table [index]));

		// If a slot is empty the row contains a number of the hash-table slot
		// and the message "The slot is empty".
		for (int i = 0; i < table.length; i++) {
			System.out.print(String.format(" %-6d| ", i));
			if (table[i] != null) System.out.print(table[i].toString() + '\n');
			else System.out.print("The slot is empty\n");
		}
	}

}
