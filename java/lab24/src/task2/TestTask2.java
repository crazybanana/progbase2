package task2;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;
import common.GeomFigure;

public class TestTask2 {

	@Test
	public void testEmptyTable() {
		OAHastable tableEmpty = new OAHastable(3);
		assertEquals(tableEmpty.size(), 0);
		assertTrue(tableEmpty.isEmpty());

		// remove from empty hashTable
		GeomFigure gf = new GeomFigure(10, 2, 3, 4);
		assertFalse(tableEmpty.remove(gf));
		assertEquals(tableEmpty.size(), 0);

		// search in empty hashTable
		assertFalse(tableEmpty.contains(gf));

	}

	@Test
	public void testAddOneElement() {
		OAHastable table = new OAHastable();
		GeomFigure gf = new GeomFigure(10 , 2, 3, 2);
		table.add(gf);
		assertEquals(table.size(), 1);
		assertFalse(table.isEmpty());
	}

	@Test
	public void testAddSomeNotUniqueElements() {
		OAHastable table = new OAHastable(5);
		GeomFigure gf = new GeomFigure(2, 3, 12, 24);
		assertTrue(table.add(gf));
		assertEquals(table.size(), 1);
		assertFalse(table.add(gf));
		assertEquals(table.size(), 1);

		GeomFigure gf2 = new GeomFigure(2,3,12,29);
		assertEquals(gf.hashCode() % 5, gf2.hashCode() % 5);
		assertTrue(table.add(gf2));
		assertTrue(table.contains(gf2));
		assertEquals(table.size(), 2);
		assertTrue(table.remove(gf2));
		assertTrue(table.contains(gf));
		assertEquals(table.size(), 1);
		GeomFigure gf3 = new GeomFigure(2, 3, 12, 24);
		assertFalse(table.add(gf3));
		assertTrue(table.contains(gf3));
		assertEquals(table.size(), 1);
		assertTrue(table.remove(gf3));
		assertEquals(table.size(), 0);
	}

	@Test
	public void testAddSomeUniqueElements() {
		OAHastable table = new OAHastable();
		GeomFigure gf = new GeomFigure(2, 3, 12, 24);
		GeomFigure gf2 = new GeomFigure(1, 2, 3, 4);
		GeomFigure gf3 = new GeomFigure(2, 3, 75, 5);
		assertTrue(table.add(gf));
		assertEquals(table.size(), 1);
		assertTrue(table.add(gf2));
		assertEquals(table.size(), 2);
		assertTrue(table.add(gf3));
		assertEquals(table.size(), 3);
	}

	@Test
	public void testRehash() {
		OAHastable table = new OAHastable(4);
		table.print();
		System.out.println();
		table.add(new GeomFigure(0, 0, 0, 0));
		assertEquals(table.size(), 1);
		table.add(new GeomFigure(0, 0, 0, 1));
		assertEquals(table.size(), 2);
		assertTrue(table.add(new GeomFigure(0, 0, 0, 5)));
		assertEquals(table.size(), 3);
		assertFalse(table.add(new GeomFigure(0, 0, 0, 5)));
		table.print();
	}

	@Test
	public void testRemove() {
		OAHastable table = new OAHastable();
		GeomFigure gf = new GeomFigure(2, 3, 12, 24);
		GeomFigure gf2 = new GeomFigure(7, 2, 3, 4);
		GeomFigure gf3 = new GeomFigure(-3, 3, 23, 5);
		table.add(gf);
		table.add(gf2);
		table.add(gf3);
		assertEquals(table.size(), 3);
		assertTrue(table.remove(gf));
		assertEquals(table.size(), 2);
		assertTrue(table.remove(gf2));
		assertEquals(table.size(), 1);
		assertFalse(table.remove(gf2));
		assertEquals(table.size(), 1);
		assertTrue(table.remove(gf3));
		assertEquals(table.size(), 0);
	}

	@Test
	public void testContains() {
		OAHastable table = new OAHastable();
		GeomFigure gf = new GeomFigure(2, 3, 12, 24);
		GeomFigure gf2 = new GeomFigure(1, 2, 3, 4);
		assertFalse(table.contains(gf));
		assertFalse(table.contains(gf2));
		table.add(gf);
		table.add(gf2);
		assertTrue(table.contains(gf));
		assertTrue(table.contains(gf2));
		table.remove(gf);
		assertFalse(table.contains(gf));
		assertTrue(table.contains(gf2));
	}

}
