package task2;

import common.FigureSet;
import common.GeomFigure;
import jdk.nashorn.internal.objects.NativeUint8Array;

/**
 * 
 * This class implements a set as a hash table. Hash table is an associative
 * array of entries. <br>
 * Each entry contains geometric figure or null. <br>
 * Hash table resolves collisions by open addressing
 *
 */
public class OAHastable implements FigureSet {

	private GeomFigure[] table;

	/**
	 * the number of non-null entries in the hashtable
	 */
	private int size;

	/**
	 * default size of the hashtable
	 */
	private final static int initialCapacity = 11;

	/**
	 * The load factor is a measure of how full the hash table is allowed to get
	 * / before its capacity is automatically increased
	 */
	private static double loadFactor = 0.75;

	private final static DelGeomFigure DELL = DelGeomFigure.getInstance();

	OAHastable() {
		this(initialCapacity, loadFactor);
	}

	/**
	 * Constructs a new, empty hashtable with the specified initial capacity and
	 * default load factor (0.75).
	 * 
	 * @param initialCapacity
	 *            the initial capacity of the hashtable
	 */
	OAHastable(int initialCapacity) {
		this(initialCapacity, loadFactor);
	}

	/**
	 * Constructs a new, empty hashtable with the specified initial capacity and
	 * the specified load factor.
	 * 
	 * @param initialCapacity
	 *            the initial capacity of the hashtable
	 * @param loadFactor
	 *            the load factor of the hashtable
	 */
	public OAHastable(int initialCapacity, double loadFactor) {
		if ((initialCapacity <= 0) || (loadFactor <= 0.0)) {
			throw new IllegalArgumentException();
		}
		table = new GeomFigure[initialCapacity];
		/*this.loadFactor */OAHastable.loadFactor = loadFactor;
	}

	/**
	 * Returns the number of entries in the hashtable
	 * 
	 * @return the number of entries in the hashtable
	 */
	@Override
	public int size() {
		return size;
	}

	/**
	 * Increases the capacity of and internally reorganizes this hashtable, in
	 * order to accommodate and access its entries more efficiently. This method
	 * is called when the number of elements in the hashtable exceeds this
	 * hashtable's capacity and load factor
	 */
	private void rehash() {
		GeomFigure[] oldTable = table;
		table = new GeomFigure[table.length + 16];
		for (GeomFigure cur : oldTable) if (add(cur)) size--;
	}

	/**
	 * The hash function is used to calculate the hasvalue of the object gf.
	 * Choose hashing method from your variant (table 1): deletion or
	 * multiplication
	 * 
	 * @param gf
	 *            some geometric figure
	 * @return hash value - index in table
	 */
	private int hash(GeomFigure gf, int probe) {
		if (gf == null) throw new NullPointerException();
		return (gf.hashCode() + (int)Math.pow(probe, 2)) % table.length;
	}

	@Override
	public boolean add(GeomFigure gf) {
		// TODO
		// if gf is not null and hashtable doesn't contain gf
		// -----calculate hashvalue
		// ------while slot is empty
		// ----------search next empty slot
		// ----------increase probe number
		if (contains(gf) || gf == null) return false;
		int probe = 0;
		do {
			int index = hash(gf, probe++);
			if (table[index] == null || table[index] == DELL) {
				table[index] = gf;
				size++;
				if (size >= (int)(table.length * loadFactor)) rehash();
				return true;
			}
		}while (probe != table.length - 1);
		return false;
	}

	@Override
	public boolean contains(GeomFigure gf) {
		if (gf == null) return false;
		int probe = 0;
		int index;
		do {
			index = hash(gf, probe++);
			if (gf.equals(table[index])) return true;
		} while (probe != table.length - 1 && table[index] != null);
		return false;
	}

	@Override
	public boolean remove(GeomFigure gf) {
		// TODO
		// if gf is not null and hashtable contains gf
		// ----calculate hashvalue of gf
		// ----search slot which object equals to gf
		// ----if such element has found
		// --------write to the slot DELL
		// --------decrease the size of hash table
		// --------return true
		if (!contains(gf)) return false;
		int probe = 0;
		do {
			int index = hash(gf, probe++);
			if (gf.equals(table[index])) {
				table[index] = DELL;
				size--;
				return true;
			}
		}while (probe != table.length - 1);
		return false;
	}

	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	@Override
	public void print() {
		// TODO
		// Output the table , where each row contains a number of the hash-table
		// slot, and the element itself.For example,

		// System.out.println(String.format(" %-6d|%s", index, table [index]));

		// If a slot is empty the row contains a number of the hash-table slot
		// and the message �The slot is empty�.
		for (int i = 0; i < table.length; i++) {
			System.out.print(String.format(" %-6d| ", i));
			if (table[i] != null) System.out.print(table[i].toString() + '\n');
			else System.out.print("The slot is EMPTY\n");
		}
	}

}

/*
 * Represents object that was be deleted from a table
 */

class DelGeomFigure extends GeomFigure {

	private static DelGeomFigure delFigure = null;

	private DelGeomFigure() {
		super(0,0,0,0);
	}

	public static DelGeomFigure getInstance() {
		if (delFigure == null) {
			delFigure = new DelGeomFigure();
		}
		return delFigure;
	}

	@Override
	public String toString() {
		return "deleted element";
	}

}
