package common;

/**
 * Class represents geometric figure such as rectangle or triangle. Contains
 * coordinates of figure and methods to calculate area and perimeter
 */
public class GeomFigure {
	private double x1;
	private double y1;
	private double x2;
	private double y2;
	private double length;
	private double angle;
	private int hash = 0;

	public GeomFigure(double x1, double y1, double x2, double y2) {
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
		this.length = Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
		if (length != 0) this.angle = Math.acos(Math.abs(x2 - x1) / length) * 180 / Math.PI;
		else this.angle = 0;
		this.hash = hashCode();
	}

	/**
	 * Returns the hash code value for this geometric figure. The hash code of a
	 * figure is defined to be the sum of the hash codes of the elements in the
	 * figure, where the hash code of a null element is defined to be zero.
	 * 
	 * @return the hash code value for this object
	 */
	public int hashCode() {
		if (hash == 0) {
			hash = 31 + Math.abs((int)x1);
			hash = hash * 31 + Math.abs((int)y1);
			hash = hash * 31 + Math.abs((int)x2);
			hash = hash * 31 + Math.abs((int)y2);
		}
		return hash;
	}

	@Override
	public String toString() {
		return String.format("%-10s| %-6g| %-6g| %-6g| %-6g| %-6g| %-6g|", hash, x1, y1, x2, y2, length, angle);
	}

	public boolean equals(GeomFigure gf) {
		return gf != null && Double.compare(x1, gf.x1) == 0 && Double.compare(y1, gf.y1) == 0 && Double.compare(x2, gf.x2) == 0 && Double.compare(y2, gf.y2) == 0;
	}

}
