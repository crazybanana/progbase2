public class Main {

    public static int recSumm(int a, int b) {
        if(a == 0 || b == 0) return 0;
        else {
            return a + recSumm(a, b - 1);
        }
    }

    public static void main(String[] args) {
        System.out.println(recSumm(5, 6));
        System.out.println(recSumm(55, 6));
        System.out.println(recSumm(8, 6));
        System.out.println(recSumm(2, 0));
        System.out.println(recSumm(0, 6));
        System.out.println(recSumm(0, 0));
    }
}
