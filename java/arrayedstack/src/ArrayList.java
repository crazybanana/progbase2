/**
 * Created by dima on 23.06.17.
 */
    /**
     * KPI- FPM - PZKS Course: Algorithms and Data Structures (2) Laboratory work 3
     *
     * @author Olena Khomenko <br>
     *
     *         Resizable-array. Implements List interface <br>
     *         This class skeleton contains methods with TODO <br>
     *         Implement methods with TODO and write addition methods to support
     *         class functionality.
     *
     *         Methods isEmpty, get, set operations run in constant time. <br>
     *         The add operation runs in amortized constant time. <br>
     *         All of the other operations run in linear time (roughly speaking).
     */

    public class ArrayList implements List {
        // The array buffer into which the elements of the ArrayList are stored. The
        // capacity of the ArrayList is the length of this array buffer

        private String[] list;

        // Default initial capacity
        private final int defaultCapacity = 10;

        // the size of the array used to store the elements in the list
        private int capacity;

        // The size of the ArrayList (the number of elements it contains)
        private int size;

        /**
         * Constructs an empty list with an initial capacity of ten
         *
         */

        public void ensureCapacity(int minCapacity){
            int current = list.length;
            if(minCapacity > current){
                String[] newList = (String[]) new Object[Math.max(current * 2, minCapacity)];
                System.arraycopy(list, 0, newList, 0, size);
                list = newList;
            }
        }

        public ArrayList() {
            this(10);
            // TODO
        }

        /**
         * Constructs an empty list with the specified initial capacity
         *
         * @param initialCapacity
         *            the initial capacity of the list
         */
        public ArrayList(int initialCapacity) {
            super();
            if(initialCapacity < 0){
                return;
            }
            this.list = new String[initialCapacity];
        }

        @Override
        public boolean add(String element) {
            if(size == list.length)
                ensureCapacity(size + 1);
            list[size++] = element;
            return true;
        }

        @Override
        public boolean add(int index, String e) {
            if(index > size || index < 0)return false;
            System.arraycopy(list, index, list, index+1, size-index);
            list[index] = e;
            return true;
        }

        @Override
        public String remove(int index) {
            if(index > size || index < 0)return null;
            String r = list[index];
            if(index != --size)
                System.arraycopy(list, index+1, list, index, size-index);
            list[size] = null;
            return r;
        }

        @Override
        public String get(int index) {
            // TODO
            if(index > size || index < 0)return null;
            return list[index];
        }

        @Override
        public String set(int index, String element) {
            // TODO
            if(index > size || index < 0)return null;
            String result = list[index];
            list[index] = element;
            return result;
        }

        @Override
        public int size() {
            return size;
        }

        @Override
        public boolean isEmpty() {
            if(size == 0) return true;
            return false;
        }
}
