import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        ArrayList myList = new ArrayList();

        myList.add("2");
        myList.add("3");
        myList.add("A");
        myList.add("B");
        myList.add("AB");
        myList.add("F");

        System.out.println("Array List:");
        for(int i = 0; i < myList.size(); i++){
            System.out.printf("%s ", myList.get(i));
        }

    }
}
