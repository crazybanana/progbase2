import java.util.NoSuchElementException;

/**
 * Created by dima on 23.06.17.
 */


public class LinkedStack {
    private SLNode top;
    private int size;

    private boolean check(){
        if(size < 0){
            return false;
        }
        if(size == 0){
            if(top != null)return false;
        }else if(size == 1){
            if(top == null)return false;
            if(top.next != null)return false;
        }else{
            if(top == null)return false;
            if(top.next == null)return false;
        }
        int numberOfNodes = 0;
        for (SLNode x = top; x != null && numberOfNodes <= size; x = x.next) {
            numberOfNodes++;
        }
        if (numberOfNodes != size) return false;
        return true;
    }

    public boolean isEmpty() {
        return top == null;
    }

    public int pop() {
        if(isEmpty()) throw new NoSuchElementException("Stack underflow");
        int data = top.data;
        top = top.next;
        size--;
        assert check();
        return data;
    }

    public void push(int item) {
        SLNode oldtop = top;
        top = new SLNode(item);
        top.data = item;
        top.next = oldtop;
        size++;
        assert check();
    }

    public int top() {
        if (isEmpty()) throw new NoSuchElementException("Stack underflow");
        return top.data;
    }

    public int size() {
        return size;
    }

    public void display()
    {
        if (size == 0)
        {
            System.out.print("Empty\n");
            return ;
        }
        SLNode ptr = top;
        while (ptr != null)
        {
            System.out.print(ptr.getData()+" ");
            ptr = ptr.getNext();
        }
        System.out.println();
    }
}
