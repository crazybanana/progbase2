import java.io.File;
import java.util.List;


public class Task1Main {
	// name of file which stores data about students
	private static String fileName = "students.csv";

	// path to the current directory that contains directory "data"
	private static String currenDir = System.getProperty("user.dir")
			+ File.separatorChar + "data";

	public static void main(String[] args) {
		BSTree<Student> dict = new BSTree<>();
		StudentReader reader = new StudentReader(fileName,currenDir);
		List<Student> students = reader.read();
		for (Student s:students){
			dict.put(s);
		}

		dict.printDictionary();
		System.out.println("removed elem = " + dict.remove());
		dict.printDictionary();

	}
}

