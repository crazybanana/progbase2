/**
 * KPI- FPM - PZKS Course: Algorithms and Data Structures (2) Laboratory work 5
 *
 * @author Olena Khomenko <br>
 *         Represents information about student: its name and number of course <br>
 *         This class is a sample how to define class, fields and methods
 *
 *         Rewrite this class and its methods <br>
 *         Choose information to be saved in a class from lab manuals (table 1,
 *         col.2).<br>
 *
 *         Write methods setXXX to set specified value to the field XXX. <br>
 *
 *         Write method print to output information about student (values of the
 *         fields) in formatted string. <br>
 *
 *         Write static methods boolean isValidXXX to check whether specified
 *         string could set (or convert and set) to the field XXX
 *
 */

public class Student {

	private int cardNumber;

	// TODO
	// add fields according to your variant
	private String name;
	private String surname;
	private int course;
	private int mark;
	private boolean dormitory;

	public void setCardNumber(int cardNumber) {
		this.cardNumber = cardNumber;
	}

	public Student(int cardNumber, String name, String surname, int course, int mark, boolean dormitory) {
		if (!isValidCardNumber(cardNumber)) throw new IllegalArgumentException("Wrong card number");
		if (!isValidName(name)) throw new IllegalArgumentException("Wrong name");
		if (!isValidSurname(surname)) throw new IllegalArgumentException("Wrong surname");
		if (!isValidCourse(course)) throw new IllegalArgumentException("Wrong course");
		if (!isValidMark(mark)) throw new IllegalArgumentException("Wrong mark");

		this.cardNumber = cardNumber;
		this.name = name;
		this.surname = surname;
		this.course = course;
		this.mark = mark;
		this.dormitory = dormitory;
	}

	public int getCardNumber() {return cardNumber;}

	public String getName(){return name;}

	public String getSurname() {return surname;}

	public int getCourse() {return course;}

	public int getMark() {return mark;}

	public boolean getDormitory() {return dormitory;}


	@Override
	public String toString() {
		return String.format("%-10d| %-10s| %-12s| %-4d| %-4d| %b ", cardNumber,name,surname,course,mark,dormitory);
	}

	public static int getCountsOfDigits(long number) {
		int count = (number == 0) ? 1 : 0;
		while (number != 0) {
			count++;
			number /= 10;
		}
		return count;
	}

	/**
	 * Determines if the specified string is valid card number: contains only
	 * one digit character
	 *
	 * @param number the string to be tested
	 * @return true if the specified string is a valid card number, false
	 * otherwise.
	 */
	public static boolean isValidCardNumber(int number) {return getCountsOfDigits(number) == 8;}

	public static boolean isValidCourse(int course){return !(course < 1 || course > 6);}

	public static boolean isValidMark(int mark){return !(mark < 0 || mark > 100);}

	public static boolean isValidName(String name){
		char[] chArray = name.toCharArray();
		for(char ch : chArray){
			if(Character.isDigit(ch)){
				return false;
			}
		}
		return true;
	}

	public static boolean isValidSurname(String surname){
		char[] chArray = surname.toCharArray();
		for(char ch : chArray){
			if(Character.isDigit(ch)){
				return false;
			}
		}
		return true;
	}
}
